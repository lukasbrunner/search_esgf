# search_esgf

A simple tool to search the Earth System Grid Federation (ESGF) for data, summarize the search results, and download the data. 


##### Users of this software should note that:

* It's a very good idea to read through this entire README before using the software.
* Additional comments and usage examples are provided in the `search.py` file.
* Changes to this software are inevitable, whether to add features or fix problems, so it's good idea to check gitlab once in a while for updates.


## Overview

Each search is contained in its own working directory, which is created by running `setup.py` (for the details, see **Getting started** below).
These working directories live in the `searches` directory.
Within each working directory is a python script, `search.py`, that the user edits to specify search criteria. 
Running `search.py` will then search ESGF, summarize the search results, and retrieve `wget` scripts to download the data.
One or more shell scripts to run the `wget` scripts are also generated. 
Running these will download the data. 

The intention is that a user creates a new working directory in order to start a project, a paper, ask a scientific question, etc.
Data don't necessarily have to be be downloaded since the summaries allow exploration of the archive prior to downloading.
For example, the user can determine how many ensemble members and/or years of a given experiment for a given variable are available, so as to find out what kind of model ensemble is available to address a particular question.
Previously used working directories can be left alone so that the user retains a record of searches done in the past.
This also helps facilitate reproducibility / repeatability of searches.
However, note that the working directories are **not** tracked by git; the user is responsible for backing them up (if desired). 

Data are downloaded by default to a local archive directory structured following CMIP6 convention (i.e. the "data reference syntax", DRS; an example of this directory structure is given below).
To progressively add to a local archive of CMIP6 data, use the same local archive directory each time a new search is done.
By default, the local archive is set to the CCCma local shared group repository of CMIP6 data, which as of **30 July 2019** is `/misc/npdata12a/ra40/data/ESGF_DOWNLOADS/`.
**This is only relevant for CCCma internal users, so users obtaining this software from the public-facing repository will need to set a different location.**
The data can be downloaded to another location if the user provides a different path either when `setup.py` is called (using the `-l` option) or by editing `search.py` in the working directory: the `local_archive` variable specifies the location for downloaded data.

The script will scan the local archive to determine what's already been downloaded and avoid downloading data that's already in the local archive.
When scanning it checks to see that any local dataset is complete, i.e. has the correct size and number of files that the ESGF search results say it should.
(In CMIP6 parlance, a "dataset" consists of one variable from one model run. 
E.g. monthly surface air temperature from the r1i1p1f1 ensemble member of the historical experiment is a single dataset. 
A dataset can be a single file or multiple files.)
Note also that the wget scripts retrieved from ESGF are supposed to not re-download data if they find matching, up-to-date data in the target directory.
So even without scanning the local archive it is *probably* safe to re-run wget scripts.
This also means that the local data can be updated at any time by re-running the wget scripts. 
However, this will only update specified versions of the data that are already downloaded.
If new versions have been published, a search for these will need to be done. 
(Note however that the default behaviour of the ESGF search is to return only the most recent version of any published dataset if no particular version is specified in the search.)
The version of a dataset is indicated in the last part of its name, e.g. for the dataset

```
CMIP6.CMIP.CCCma.CanESM5.historical.r21i1p1f1.Amon.tasmin.gn.v20190429
```

the version is `v20190429`. 
The version is part of the standard CMIP6 data path, which for this example would be

```
CMIP6/CMIP/CCCma/CanESM5/historical/r21i1p1f1/Amon/tasmin/gn/v20190429/
```

Within `search_esgf` the name of a dataset always includes the version.
So, for example, if a new version of the above dataset were published (e.g. `CMIP6.CMIP.CCCma.CanESM5.historical.r21i1p1f1.Amon.tasmin.gn.v20190610`), a wget script already obtained for the previous version (`v20190429`) will not get the new version of the data. 
A wget script for the new version could be obtained by redoing the search. 
By default the search will only return results for the latest published version of any dataset.
Hence rerunning a previous search, as long as no specific version has been specified in the search criteria, should return results for only the latest version of any dataset.


## Getting started

In the directory where you want put the software, clone the repository (a directory called `search_esgf` containing the software will be created).
If you are getting the software from the internal ECCC gitlab.science.gc.ca repo, use

```
git clone git@gitlab.science.gc.ca:rja001/search_esgf.git
```

or if you're getting it from the public-facing repo at gitlab.com (which is just a public mirror of the gitlab.science.gc.ca one), use

```
git clone https://gitlab.com/JamesAnstey/search_esgf.git
```

**CCCma internal users:** it's advisable to invoke the `cccanpy` environment to ensure that required python modules are available (however, note that `search_esgf` is currently written only for python 2).
Then go to the main directory of the repository and do:

```
cd search_esgf
```

In this directory, at the linux shell prompt run the following command to create a new working directory:

```
./setup.py newproject
```

(where "newproject" is just whatever you want to call your new working directory). Then change to the to new working directory:

```
cd searches/newproject
```

and edit `search.py` as needed to define your search. 
Then run the search by doing:

```
./search.py
```

This will execute the search and provide a summary of the results, and also do an inventory of the local archive, which is the directory specified by the `local_archive` variable at the top of `search.py` (see below for more info).
`search.py` could also be run within a python or ipython session, e.g. by doing

```
execfile('search.py')
```

at the python/ipython session prompt. 
This is a useful way to run the software because the search results and other info are then available as python variables within the python/ipython session. 
Note, to quickly see if it's working out-of-the-box, you can run `search.py` leaving `examples=True` before editing anything. 

Program execution is controlled by the flags at the top of `search.py`, for which the default settings are:

```
search_esgf = True
summarize_search_results = True

do_local_inventory = True
summarize_local_inventory = True
check_if_already_downloaded = True

retrieve_wget_scripts = not True
prepare_download = not True
```

These flags can be set to `True` or `False` in any combination.
A list called `datasets` is produced that specifies the names of datasets to be downloaded.
The `selectors` list can be used, if desired, to provide filters that select a subset of the search results for download (examples of filters are given in the script).
That is, the `datasets` list is filtered according to the filters given in `selectors`.
Note there is an additional option to retain only a specified number of ensemble members (without having to know the `member_id` of those ensemble members beforehand; an example is in the script).
The `selectors` list will also restrict the search of the local archive to look for only those variables allowed by the filters.
This can save time on the search if the local archive is very large.
The summary of the local archive will then only include those variables (i.e. this allows a selective summary of the local archive, so that the user can just check for fields of interest without having to see everything in the archive).

Downloading data requires setting the flags `retrieve_wget_scripts` and `prepare_download` to `True`.
With `retrieve_wget_scripts` set to `True`, wget scripts to download the data will be retrieved from ESGF and placed in the `wget` subdirectory.
With `prepare_download` is set to `True` then directories for the new data will be created in the local archive and a shell script (by default named `download_datasets.sh`) is created to download the data.
(The reason these two flags are not `True` by default is that retrieving wget scripts can take a little while if there are a lot of datasets to download, and to avoid creating unnecessary directories in the local archive.)
Then to actually download the data, run the shell script(s) in the working directory by using this command at the shell prompt:

```
./download_datasets.sh
```

If multiple shell scripts for downloading have been written, they'll be named `download_datasets_[some info].sh`, e.g. `download_datasets_Amon.sh`. 
(See examples in `search.py` for how multiple shell scripts can be created.
This can be useful to organize the downloads, e.g. to have separate scripts for different fields and then run them in parallel.)
The first time you run a search you'll be asked to enter your OpenID credentials (see **OpenID** below for more info).
The data should then download into your chosen `local_archive` directory.
Progress will be displayed on the standard out as the download script runs.


## OpenID

An OpenID is required to download data. 
The first time you run a wget script, it will ask for your OpenID credentials. 
It then remembers them for subsequent downloads, although after a few(?) days it seems to forget and you have to re-enter them again.
An OpenID can be created at one of the ESGF main data nodes, e.g.

> https://esgf-node.llnl.gov/projects/esgf-llnl/

go to "Create Account" at top right and follow the steps. 

When you run a wget script, it will ask for your credentials.
For example:

```
Please give your OpenID:  https://ceda.ac.uk/openid/James.Anstey
Please give your username if different [James.Anstey]:  janstey
MyProxy Password?  [enter your password]
```

Info on your credentials will be stored in a subdirectory of your home directory (`~/.esg`). 
This should be automatically created the first time you run a wget script.

Note, in some cases certificates may not work (e.g. on the ppp machines they don't because the version of java is too old).
In this case it's possible to avoid using them by setting the `certificate method` option in the call to the `write_wget_script()` function - see the `search.py` script where this function is called to see how.


## Creating and updating a local data archive

The location of a local data archive can be specified when setting up a project, e.g.

```
./setup.py newproject -l /HOME/user/data/
```

Here "l" (this is a lowercase "L") stands for "local".
If the user always uses the same local data archive when setting up new searches then downloaded data will be progressively added to this archive.
This can be done to build up a local collection of CMIP6 data. 
**CCCma internal users:** it is strongly recommended to use the default location, the CCCma shared group archive, so as to make best use of our limited local disk space (i.e. to not repeat downloads already done by others, and hopefully save yourself some work).
The local archive path can also be set by changing the `local_archive` variable in `search.py`.


## Updating to the latest version of the software

If you've already cloned the software (see **Getting started** above) but want to update it to the latest version on gitlab, execute these git commands in the `search_esgf` directory:

```
git fetch --all
git pull
```

This should bring you up to date, as can be verified by doing the git command

```
git status

```

**Note re. backward compatibility:** an effort has been made when updating this software to preserve backward compatibility with earlier versions of the `search.py` template script located in the `search_esgf/bin` directory.
This hopefully means that existing versions you have of `search.py` in your working directories (i.e. subdirectories of the `search_esgf/searches/` folder) will still work the same way when the software is updated.
But, you know, accidents happen.
If updating `search_esgf` causes a problem with an existing `search.py` script, first try a diff of your script with the template version of `search.py` located in `search_esgf/bin`.
Most likely the change is something simple that you can correct by inspecting the differences.
If that fails, remember it is always possible with git to roll back to an earlier version of the software.
Note that every time `setup.py` is run to create a new working directory, the new copy of `search.py` that is created in the working directory contains the git commit hash of the latest version of the software (this is the `latest_git_commit` variable).
This can be used if necessary to roll back to the software version that originally created that `search.py` script.
