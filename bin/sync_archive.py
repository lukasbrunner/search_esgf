#!/usr/bin/env python


local_archive = '/misc/npdata12a/ra40/data/ESGF_DOWNLOADS/'

remote_machine = ''
remote_archive = '/HOME/rja/data/CMIP6/test_remote_archive_sync/'

remote_machine = 'cccma'
remote_archive = '/home/rja/data/test_archive_sync'

# If a username is required when connecting via ssh to the remote machine,
# include that in remote_machine
remote_machine = 'rja001@sci-eccc-in.science.gc.ca'
remote_archive = '/fs/home/fs1/ords/crd/ccrn/rja001/test_archive_sync'


do_local_inventory = True
summarize_local_inventory = True
sync_to_remote = True


selectors = []
sel = {
    'source_id'     : 'CanESM5',
    'experiment_id' : 'historical',
    'table_id'      : 'Amon',
    'variable_id'   : 'tas',
    'member_id'     : ['r1i1p1f1', 'r2i1p1f1'],
}
selectors.append(sel)
sel = {
    'source_id'     : 'CanESM5',
    
    'experiment_id' : 'ssp245',
    #'experiment_id' : 'ssp370',

    'table_id'      : 'Amon',
    'variable_id'   : 'tas',
    'member_id'     : ['r3i1p1f1'],
}
selectors.append(sel)



###############################################################################
import os
import time
import esgfsearch as es
reload(es)

try: d_local
except: d_local, d_info_local = {},{}
###############################################################################

if do_local_inventory:
    d_disk = es.check_disk_usage(local_archive)
    d_local, d_info_local = es.inventory(local_archive, selectors)

if summarize_local_inventory:
    axes = {
        # These axes will produce a table summarizing how much data is
        # available within each CMOR table (aka MIP table) for experiments
        # defined by each endorsed MIP.
        'row'       : ['activity_drs'],
        'column'    : ['table_id'],
    }
    # Specify tables to show. This specifies the quantity that will be shown
    # in the table entries.
    tables = []
    tables.append('no. of datasets')
    tables.append('total size of datasets')
    es.summarize(d_local, d_info_local, axes, tables, summary_dir=None)

datasets = sorted(d_local.keys())
    
if sync_to_remote:

    do_cmds = True # set False to just see the commands, not execute them

    nd = len(datasets)
    total_size = sum([d_local[dataset]['doc']['size'] for dataset in datasets])
    te = time.time()
    print('\nSynchronizing {} datasets from local to remote, total size: {}'\
        .format(nd, es.file_size_str(total_size)))
    for n, dataset in enumerate(datasets):
        doc = d_local[dataset]['doc']
        local_path  = os.path.normpath(doc['abspath'])
        assert local_path.startswith(local_archive), 'dataset path not in specified local archive'
        assert local_path.split(local_archive)[-1] == doc['relpath'], 'incorrect relative path for dataset'
        remote_path = os.path.join(remote_archive, doc['relpath'])
        lc = []
        cmd = 'mkdir -p {}'.format(remote_path)
        if remote_machine != '':
            cmd = 'ssh {} {}'.format(remote_machine, cmd)
        lc.append(cmd)
        if remote_machine != '':
            remote_path = '{}:{}'.format(remote_machine, remote_path)
        cmd = 'rsync -tpur {}/* {}'.format(local_path, remote_path)
        lc.append(cmd)
        print('\nSynchronizing dataset {} of {} to remote location: {} (size: {}, no. of files: {})'\
            .format(n+1, nd, dataset, doc['size_str'], doc['number_of_files']))
        for cmd in lc:
            print(cmd)
            if do_cmds: 
                os.system(cmd)
        #total_size += doc['size']

    te = time.time() - te
    local_path  = os.path.normpath(local_archive)
    remote_path = os.path.normpath(remote_archive)
    if remote_machine != '':
        remote_path = '{}:{}'.format(remote_machine, remote_path)
    print('\nFinished synchronizing {} datasets from'.format(nd))
    print(  '  {}'.format(local_path))
    print(  'to')
    print(  '  {}'.format(remote_path))
    print(  'Total size of synchronized data: {}'.format(es.file_size_str(total_size)))
    fmt = '%.2f'
    print(  'Elapsed time for synchronizing: {} s ({} min; {} hr)'\
        .format(fmt % te, fmt % float(te/60.), fmt % float(te/3600.)))





