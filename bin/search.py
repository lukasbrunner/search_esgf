#!/usr/bin/env python
'''
Script to search ESGF for datasets according to user-specified criteria.
Essentially same thing as using the point-click search website:
    https://esgf-node.llnl.gov/search/cmip6/
but using scripting to automate the searches. This is made possible by
the ESGF Search RESTful API:
    https://github.com/ESGF/esgf.github.io/wiki/ESGF_Search_REST_API

This script provides a basic template that users can change as needed to suit
their needs.

'''

# Locations for downloaded data & summaries of search results
local_archive = './data'
summary_dir = './summary'

# Flags to control script execution
search_esgf = True
summarize_search_results = True

do_local_inventory = True
summarize_local_inventory = True
check_if_already_downloaded = True

retrieve_wget_scripts = not True
prepare_download = not True


# Search filters
#
# "filters": is a list of dicts, each of which defines a search by giving a
# set of search parameters (also referred to as search facets). A "dataset"
# in CMIP6 parlance refers to one output field from one model run (e.g. 
# surface temperature from one ensemble member of the historical experiment
# from one model is a single "dataset"). The following searchable parameters
# identify datasets:
#
#   parameter           example value
#   ---------           -------------
#   'mip_era'           'CMIP6'
#   'activity_drs'      'ScenarioMIP'
#   'institution_id'    'CCCma'
#   'source_id'         'CanESM5'
#   'experiment_id'     'ssp245'
#   'member_id'         'r1i1p1f1'
#   'table_id'          'Amon'
#   'variable_id'       'tas'
#   'grid_label'        'gn'
#   'version'           'v20190429' (see comments below re. version)
# 
# Any of these parameters can be specified in the filter dicts in the 
# "filters" list. The parameters narrow down the search, so to search for
# all possible values of a given parameter just leave that parameter out.
# For example: to search for all available ensemble members, leave out the
# 'member_id' parameter.
#
# A dataset is also identified by its version, which is an 8-digit integer
# giving year,month,day in the format YYYYMMDD, or equivalently in string
# form, 'v' followed by the integer, e.g. 'v20190429'.
# If no version is specified then by default the search will look for the
# most recent version of any given dataset. To search for a specific
# version the user can specify the version, or provide a range by giving
# min,max version dates (as is done under "More Search Options" on the
# ESGF website). For example:
#   'min_version'   : 20190301,
#   'max_version'   : 20190424,
# or equivalently,
#   'min_version'   : 'v20190301',
#   'max_version'   : 'v20190424',
# This works as <=, >= operators, e.g. min_version=20190306 will return a
# version v20190306, but not v20190305. Or to give just a specific version,
#   'version'       : 'v20190429'
# or equivalently,
#   'version'       : 20190429
# Note that if the version is given as a string then it MUST be in the
# format 'vYYYYMMDD', and if given as an integer then it MUST be an 8-digit
# integer in the format YYYYMMDD. Anything else will be rejected (e.g. a
# string giving just the integer, such as '20190429', is not ok).
#
# Any number of filters (dicts) can be concatenated into list "filters".
# All of them will be searched and the results will be consolidated to remove
# any redundancies (i.e. the same dataset won't be counted / downloaded more
# than once).

filters = []

examples = True
if examples:

    example1 = not True
    example2 = True
    example3 = not True
    example4 = not True

    if example1:
        # Search parameters can be specified as a set of search parameters.
        # Either single parameter values (string) or lists (of strings) can be used.
        f = {   
            'variable_id'   : ['ua','ta'],
            'table_id'      : 'AERmonZ',
            'source_id'     : ['CESM2', 'CESM2-WACCM'],
            }
        filters.append(f)

    if example2:
        # Another way to specify a search is to give the name of a specific
        # dataset (or a list of datasets). The dataset names have to be in the
        # standard CMIP6 format, and should include the version string (as the
        # following examples indicate).
        f = {'dataset' : ['CMIP6.CMIP.CCCma.CanESM5.historical.r14i1p1f1.Lmon.gpp.gn.v20190429',
                          'CMIP6.CMIP.CCCma.CanESM5.historical.r15i1p1f1.Lmon.gpp.gn.v20190429']}
        filters.append(f)
        f = {'dataset' : 'CMIP6.ScenarioMIP.CCCma.CanESM5.ssp126.r2i1p1f1.Lmon.gpp.gn.v20190429'}
        filters.append(f)
        f = {'dataset' : 'CMIP6.CMIP.CCCma.CanESM5.historical.r21i1p1f1.Amon.tasmin.gn.v20190429'}
        filters.append(f)
        f = {'dataset' : 'CMIP6.CMIP.CCCma.CanESM5.historical.r20i1p1f1.Amon.tas.gn.v20190429'}
        filters.append(f)    

    if example3:
        # Example showing how to search for monthly-mean surface temperature
        # for all ensemble members of the historical and ssp126 experiment,
        # for all models.
        f = {   
            'experiment_id' : ['historical', 'ssp126'],
            'variable_id'   : 'tas',
            'table_id'      : 'Amon',
            }
        filters.append(f)

    if example4:
        # Example showing how to search for monthly-mean surface temperature
        # and precipitation for one ensemble member of the piControl experiment,
        # for all models.
        f = {   
            'experiment_id' : 'piControl',
            'variable_id'   : ['tas', 'pr'],
            'table_id'      : 'Amon',
            'member_id'     : 'r1i1p1f1',
            }
        filters.append(f)
        # Note, the ensemble member is identified here by its exact member_id.
        # There's no guarantee that "r1i1p1f1" is available for a given model
        # and experiment. A search over all ensemble members (i.e. leave out
        # the "member_id" parameter) should find all the available member_id
        # values for a given model and experiment.

###############################################################################
import sys
import os
bin_dir = '../../bin'
if bin_dir not in sys.path: sys.path.append(bin_dir)
import esgfsearch as es
reload(es)

es.setup_local_archive(local_archive)
d_disk = es.check_disk_usage(local_archive)

wget_dir = './wget'

# Latest git commit hash when this version of search.py was created:
latest_git_commit = ''

machine = os.uname()[1]

# Ensure the following dicts are defined. If they've been defined by
# previously running the script (in an interactive python session) then
# leave them alone.
try: d_found
except: d_found, d_info_found = {},{}
try: d_local
except: d_local, d_info_local = {},{}
###############################################################################


if search_esgf:
    # Search ESGF.
    d_found, d_info_found = es.search(filters)


if summarize_search_results or summarize_local_inventory:
    # Configure how the summary of search results and/or local inventory will
    # be presented.
    #
    # User can configure the type of summary that is displayed by adjusting
    # the "axes" and "tables" variables. "tables" specifies what we want to
    # know, and "axes" specifies how to present it (i.e. how the information
    # is organized).
    
    # Specify axes to use for each table.
    #
    # An axis can show a single parameter or a combination of parameters.
    # For a combination of parameters, each axis value in the table (i.e. row
    # or column) is a unique combination of the parameter values. 
    #
    # Two examples are given, but the user can adjust these axes to produce
    # whatever kind of table summary of the search results is desired.
    axes = {
        # These axes will produce a table summarizing how much data is
        # available within each CMOR table (aka MIP table) for experiments
        # defined by each endorsed MIP.
        'row'       : ['activity_drs'],
        'column'    : ['table_id'],
    }
    axes = {
        # These axes will produce a table show how much data is available
        # for each unique variable (which is defined by the combination of
        # 'table_id' and 'variable_id', e.g. "Amon.tas") for each 
        # experiment/model combination. Hence it aggregates results over all
        # ensemble members, but if the user wanted to show each ensemble
        # member as a separate entry, the row could instead be defined as
        #   'row' : ['source_id', 'experiment_id', 'member_id']
        'row'       : ['source_id', 'experiment_id'],
        'column'    : ['table_id', 'variable_id'],
    }
    
    # Specify tables to show. This specifies the quantity that will be shown
    # in the table entries.
    tables = []
    tables.append('no. of datasets')
    tables.append('total size of datasets')
    tables.append('unique values of member_id')
    tables.append('unique values of experiment_id')
    tables.append('estimated no. of years')
    tables.append('no. of ensemble members')


if summarize_search_results:
    # Create the summary tables for the ESGF search.
    es.summarize(d_found, d_info_found, axes, tables, summary_dir)


# Now specify the datasets to download.
selectors = []
# To download everything, use all keys of d_found:
datasets = sorted(d_found.keys(), key=es.lowercase)
# Otherwise, populate the "datasets" list with some subset of d_found keys.
# (Examples are provided below.)

dataset_selection_example = not True
if dataset_selection_example:
    # To narrow down the list of datasets, selectors can be applied.
    # These work the same way as the search filters, but are applied to the
    # list of datasets. Each entry of list "selectors" is applied independently
    # to the input list of datasets.
    # Example of a selector:
    sel = {
        'table_id'      : 'Amon',
        'experiment_id' : 'historical',
    }
    selectors.append(sel)

    # Subset the 'datasets' list based on the filters.
    datasets = es.select(selectors, d_found, datasets)

ensemble_selection_example = not True
if ensemble_selection_example:
    # Sometimes we want a specified number of ensemble members but don't know
    # what they're called, i.e. what is the member_id for each. es.ensem() will
    # select up to n ensemble members for each model+experiment combo. E.g. to
    # get just the "first" ensemble member for each, set n=1.
    # The order of ensemble members (what determines which is the "first" one)
    # is determined by sorting the list of available members using the
    # es.sort_member_id() function.
    datasets = es.ensem(datasets, d_found, n=1)


if do_local_inventory:
    # Scan the local archive directory to find out what datasets are in it.
    # This can be used to limit what is downloaded. 
    # Note that the search is filtered by the selectors specified above.
    # If the selectors list is empty, no filtering is done (i.e. the whole
    # local archive is scanned). The only reason to filter is to speed up the
    # scan.
    d_local, d_info_local = es.inventory(local_archive, selectors)

if summarize_local_inventory:
    # Show summary of the local inventory, in same format as used for
    # summarizing the ESGF search.
    es.summarize(d_local, d_info_local, axes, tables, summary_dir)

if check_if_already_downloaded:
    # Filter out datasets that already exist in the local inventory.
    #
    # NOTE: the wget scripts from ESGF work in "update" mode. If a downloaded
    # dataset is already current, the wget script won't download it again.
    # If user wants to ensure this check is done, comment out the next
    # line so that wget is run for all datasets even if they're already
    # downloaded. (Or just set do_local_inventory=False.)
    datasets = es.check_local(datasets, d_found, d_local)

if retrieve_wget_scripts:
    # Use the search results to generate wget scripts that can be used
    # to download the data.
    d_wget_scripts = es.retrieve_wget(d_found, wget_dir, datasets)
else:
    # Populate d_wget_scripts by looking to see what's in wget_dir.
    # (d_wget_scripts will be needed if prepare_download=True)
    d_wget_scripts = es.inventory_wget(wget_dir, datasets)


if prepare_download:
    # Specify location for the downloaded data, and create a shell script
    # that will download the data. 
    #
    # Dict d_output_paths, keyed by dataset, will specify the directory
    # in which the data files for each dataset will be placed. This dict
    # is populated by the es.setup_output_paths() function, which creates
    # the directory structure specified by the CMIP6 protocol.
    #
    # (If a flat directory structure is desired, simply set this dict
    # to contain the same path for every dataset. Or the user could tailor
    # it to whatever is wanted. But for the shared group archive of CMIP6
    # data, the official CMIP6 directory structure should be used!)

    d_output_paths = es.setup_output_paths(local_archive, datasets)


    # Write shell script(s) that user can invoke to download the data.
    # The default is to write a single shell script that will execute all wget
    # scripts. This is the simplest option, and users content with it can 
    # stop reading now.
    #
    # Alternatively, user can specify in dict "split_script" how to split up
    # the downloads into multiple shell scripts. This can be useful to organize
    # the downloading, perhaps running some jobs in parallel, etc. 
    split_script = {} # split_script={} ==> all datasets will be downloaded by a single script.

    split_script_example = not True
    if split_script_example:
        # There are two ways to specify how to split up the download scripts.
        # The first is the list "filters" in split_script. Each dict in this
        # list works the same way as the filter dicts specified in the
        # "filters" list that does the ESGF search. Each dict in the list
        # produces a separate shell script containing the datasets satisfying
        # the filtering criteria.
        split_script = {'filters' : []}
        f = {   
            'variable_id'   : ['tasmin', 'gpp'],
            'experiment_id' : 'historical',
        }
        split_script['filters'].append(f)
        # After any available filters in split_script['filters'] are applied,
        # remaining datasets can be grouped by unique values of the parameters
        # specified in split_script['parameter set']. For example, to put all
        # variables from each CMOR table (aka MIP table) into a single script,
        # use:
        #    split_script['parameter set'] = ['table_id']
        # To put each unique variable into its own script (e.g. so that a
        # given variable for all model runs appeared in the same script) use:
        #    split_script['parameter set'] = ['table_id', 'variable_id']
        split_script.update({
            'parameter set'     : ['table_id'],
            #'parameter set'     : ['table_id', 'variable_id'],
        })

    opt = {
        'local archive' : local_archive,
        'disk info'     : d_disk,

        # If the download is having trouble with certificates, try uncommenting
        # the following line (it should avoid the certificate check):
        #'certificate method' : '-i',
    }
    if machine in ['eccc1-ppp1', 'eccc1-ppp2']:
        # On the ppp's java is too old for ESGF certificates to work.
        openid, openid_pw = es.get_openid()
        opt.update({'certificate method' : 'pass OpenID', 'OpenID' : openid, 'OpenID password' : openid_pw})
        
    # Create shell script(s) to download data.
    es.write_wget_script(d_wget_scripts, d_output_paths, datasets, split_script, d_found, opt)



