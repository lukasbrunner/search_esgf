#!/usr/bin/env python
'''
Module containing functions used in carrying out ESGF searches, and also to
summarize the results and download the data.

The search.py script is a front end for calling the functions contained in this
module.
'''

import os
import json
import urllib
import datetime
import time
import numpy as np
import copy
import csv
import pprint
import re
import pickle
try: import openpyxl as xp
except: pass

# 'directory_format_template_' is one of the metadata parameters. It should be the same for every search. It gives
# the order of parameters in the output dir, and also (after removing the root dir) the dataset id. Example of a
# dataset id:
#   CMIP6.CMIP.NCAR.CESM2.historical.r6i1p1f1.AERmonZ.ua.gn.v20190308
directory_format_template_ = [u'%(root)s/%(mip_era)s/%(activity_drs)s/%(institution_id)s/%(source_id)s/%(experiment_id)s/%(member_id)s/%(table_id)s/%(variable_id)s/%(grid_label)s/%(version)s']
OUTPUT_PATH_TEMPLATE = directory_format_template_[0].strip('%(').strip(')s').split(')s/%(')
OUTPUT_PATH_TEMPLATE = [str(s) for s in OUTPUT_PATH_TEMPLATE]
DATASET_TEMPLATE = [s for s in OUTPUT_PATH_TEMPLATE if s not in ['root']]
del directory_format_template_ # not used again, so don't clutter the module with it

s = '<variable_id><table_id><source_id><experiment_id><member_id><grid_label>'
OUTPUT_FILE_TEMPLATE = tuple(s.strip('<').strip('>').split('><'))

SEP_DATASET = '.' # string separator used in dataset name
SEP_FILENAME = '_' # string separator used in data file name

LOCAL_ARCHIVE_PERMISSIONS = 0775

TIME_FMT_DOC = '%Y-%m-%dT%H:%M:%SZ' # format used for datetime_start,datetime_stop in the 'doc' dict returned by the ESGF search (e.g. '1850-01-16T12:00:00Z')

###############################################################################
def chksme(x):
    '''Return unique entries of list x (filter out duplicate entries).'''
    return [x[m] for m in filter(lambda m: x[m] not in x[:m], range(len(x)))]
###############################################################################
def file_size_str(a):
    '''Given file size in bytes, return string giving the size in nice
    human-readable units (like ls -h does at the shell prompt.'''
    m = 1024.
    d_b = {
        'B' : 1.
    ,   'KB': 1 / m
    ,   'MB': 1 / m**2
    ,   'GB': 1 / m**3
    ,   'TB': 1 / m**4
    ,   'PB': 1 / m**5
    }
    # list of units, in order of descending size of the unit
    uo = sorted([(d_b[s], s) for s in d_b])
    # choose the most sensible size to display
    for tu in uo:
        if (a*tu[0]) > 1: break
    su = tu[1]
    a *= tu[0]
    sa = str('%.3g' % a)
    return sa + ' ' + su
###############################################################################
def lowercase(s):
    if isinstance(s, tuple):
        s = tuple([lowercase(v) for v in s])
    elif isinstance(s, str):
        s = str.lower(s)
    elif isinstance(s, unicode):
        s = unicode.lower(s)
    else:
        assert False, 'type not recognized: ' + s.__class__.__name__
    return s
############################################################################### 
def carve(s, lc=[]):
    '''Convenience function to apply string split() method sequentially for a
    series of strings.'''
    ls = [s]
    if isinstance(lc, (str,unicode)): lc = [lc]
    assert isinstance(lc, (list, tuple))
    assert isinstance(s, (str, unicode))
    for c in lc:
        l = []
        for s in ls: l += s.split(c)
        ls = l
    return ls
###############################################################################    
def dfps(d, e):
    '''As update() method of dicts, but does not clobber existing keys of d.
    Useful to set defaults.'''
    for k in e:
        if k not in d:
            d[k] = e[k]
###############################################################################
def column_fmt(row, gap=' '*4, underline=[], emptyline=[], indent='', max_len=100, underline_case='', hl=[]):
    '''
    Display information in a nice plaintext table.
    '''
    w = ''
    if not isinstance(row, list):
        print('Input variable "row" must be list.')
        return w
    for col in row:
        for k in range(len(col)):
            s = str( col[k] )
            l = [s]
            if len(s) > max_len:
                l = []
                while len(s) > max_len:
                    s0 = s[:max_len]
                    sep = ' '
                    m = s0[::-1].find(sep)
                    if m == -1:
                        l += [ s0 ]
                        s = s[max_len:]
                    else:
                        m = len(s0)-m-1
                        l += [ s0[:m] ]
                        s = s[m+len(sep):]
                if len(s) > 0:
                    l += [s]
            col[k] = '\n'.join(l)
    ncol = len(row[0])
    for col in row:
        if len(col) != ncol: raise
    col_len = np.zeros(ncol, 'int')
    for col in row:
        for k in range(ncol):
            s = col[k]
            sl = s.split('\n')
            for s in sl:
                col_len[k] = max(col_len[k], len(s))
    l = []
    if underline_case in ['whole column']:
        colwc = []
        for m in col_len:
            colwc += ['-'*m]
    for k in range(len(row)):
        col = row[k]
        l += [ col ]
        if k in underline:
            col0 = []
            for s in col:
                col0 += ['-'*len(s)]
            l += [col0]
            if underline_case in ['repeat first']:
                # take the first underline and repeat it after every subsequent row
                colru = col0
                kru = k
        if         underline_case in ['repeat first']:
            if k > kru:
                l += [colru]
        elif    underline_case in ['whole column']:
            l += [colwc]
        if k in emptyline:
            col0 = []
            for s in col:
                col0 += ['']
            l += [col0]        
    row = l    
    use_lr = True
    if use_lr:
        lr = []
        wr = ''
    for k in range(len(row)):
        col0 = row[k]
        col1 = []
        more_cols = True
        while more_cols:
            w += indent
            if use_lr: wr += indent
            more_cols = False
            for n in range(ncol):
                s = col0[n]
                m = s.find('\n')
                if m != -1:
                    col1 += [ s[m+1:] ]
                    more_cols = True
                    s = s[:m]
                else:
                    col1 += [ '' ]
                fmt0 = '{:<'+str(col_len[n])+'}'
                s = fmt0.format(s)
                w += s + gap
                if use_lr: wr += s + gap
            w += '\n'
            if use_lr:
                lr += [wr]
                if k in hl:
                    # Put a horizontal line underneath. 
                    # If a row spans more than one row of text, this doesn't place the line correctly (3apr.17).
                    lr += ['-'*len(wr)]
                wr = ''
                
            col0 = col1
            col1 = []
    if use_lr:
        w = ''
        for wr in lr: 
            w += wr + '\n'
    return w
###############################################################################
def sort_member_id(lm):
    '''
    This is pretty OCD but it really bugs me to see r10i1p1f1 appear before
    r1i1p1f1 in a list of ensemble members. This function sorts a list (lm) of
    member_id values (e.g. 'r1i1p1f1') in order of the integers given for each
    of the r,i,p,f parts.
    
    In CMIP6, the member_id is either something like 'r1i1p1f1' or, if a
    parameter called sub_experiment_id indicating the start year is defined
    (e.g. "s1960", used for decadal predictions), then member_id can have the
    format 's1960-r1i1p1f1'.
    '''
    use_regex = True
    if use_regex:
        parts = ['s','r','i','p','f']
        optional_parts = ['s']
        required_parts = [p for p in parts if p not in optional_parts]
        # sort_order = order of precedence for sorting
        #sort_order = list(parts)
        sort_order = parts[::-1]
    else:
        parts = ['r','i','p','f'] # member_id is of form 'r1i1p1f1'

    lt = []
    lt_subexpt = []
    ls_unparsed = []
    for sm in lm:
        if use_regex:
            # use regex instead (26aug.19)
            ok = True
            d = {}
            for p in parts:
                l = re.findall(p + '\d+', sm)
                if len(l) == 0 and p in optional_parts:
                    continue
                else:
                    ok = len(l) == 1
                if not ok: break
                s = l[0]
                ok =    s.count(p) == 1 \
                    and s.startswith(p)
                if not ok: break
                s = s.strip(p)
                ok = s.isdigit()
                if not ok: break
                d[p] = int(s)
            if not ok:
                ls_unparsed += [sm]
                continue
            t = tuple([int(d[p]) for p in sort_order if p in d] + [sm])
            # Reconstruct the member_id
            sr = ''
            fmt = '%i'
            p = 's'
            if p in d: sr += p + str(fmt % d[p]) + '-'
            sr += ''.join([p + str(fmt % d[p]) for p in required_parts])

        else:
            # previous way (before 26aug.19)
            if not all([sm.count(p) == 1 for p in parts]):
                # Each part of the member_id must appear
                ls_unparsed += [sm]
                continue
            lp = carve(sm,parts)
            t = tuple([int(m) for m in lp[1:]] + [sm])
            # Reconstruct the member_id
            m = 1
            sr = ''.join( lp[:m] + [parts[k] + lp[k+m] for k in range(len(parts))] )

        # Verify that the member_id can be reconstructed (this ensures that
        # it has been parsed correctly)
        if sr == sm:
            if sm.startswith('s'):
                # This indicate that sub_experiment_id is defined
                lt_subexpt += [t]
            else:
                lt += [t]
        else:
            ls_unparsed += [sm]
    lt         = [t[-1] for t in sorted(lt)]
    lt_subexpt = [t[-1] for t in sorted(lt_subexpt)]
    ls_unparsed.sort(key=lowercase)
    lt = lt + lt_subexpt + ls_unparsed
    assert len(lt) == len(lm), 'Error in member_id sort' # make sure none go missing!
    return lt
###############################################################################
def sort_datasets(ld, member_index=None):
    '''
    Similar to sort_member_id(), this will sort a list of dataset names taking
    proper account of the ensemble members' order.

    Parameters
    ----------
    ld : list
        List of strings giving dataset names.
    member_index : int
        Index in dataset name at which the member_id occurs.
        By default this follows the CMIP6 DRS. However it can be changed
        in order to sort a list of partial dataset names where member_id
        doesn't occur in the usual place. E.g. for a list of names like
            'CanESM5.ssp370.r10i1p1f1'
        set member_index=2. Dataset names do have to have their component
        parts separated by the module variable separator string SEP_DATASET.
    '''
    if member_index is None:
        member_index = DATASET_TEMPLATE.index('member_id')
    #parts = ['s','r','i','p','f']
    parts = ['r','i','p','f','s']
    optional_parts = ['s']
    required_parts = [p for p in parts if p not in optional_parts]
    # sort_order = order of precedence for sorting
    sort_order = parts[::-1]
    lt = []
    for sd in ld:
        td = sd.split(SEP_DATASET)
        assert member_index < len(td), 'index of member_id is out of range'
        sm = td[member_index]
        ok = True
        d = {}
        for p in parts:
            l = re.findall(p + '\d+', sm)
            if len(l) == 0 and p in optional_parts:
                continue
            else:
                ok = len(l) == 1
            if not ok: break
            s = l[0]
            ok =    s.count(p) == 1 \
                and s.startswith(p)
            if not ok: break
            s = s.strip(p)
            ok = s.isdigit()
            if not ok: break
            d[p] = int(s)
        if not ok:
            continue
        t = tuple([int(d[p]) for p in sort_order if p in d] + [sm])
        # Reconstruct the member_id
        sr = ''
        fmt = '%i'
        p = 's'
        if p in d: sr += p + str(fmt % d[p]) + '-'
        sr += ''.join([p + str(fmt % d[p]) for p in required_parts])
        if sr == sm:
            # Reconstruction is ok ==> member_id was correctly parsed
            td[member_index] = t
        td = tuple(td)
        lt.append(td)
    lt = sorted(lt)
    ld = []
    for td in lt:
        t = td[member_index]
        td = list(td)
        td[member_index] = t[-1]
        sd = SEP_DATASET.join(td)
        ld.append(sd)
    return ld
###############################################################################
def fpr(f, indent='', return_str=False):
    '''Pretty print of a filter dict f. Only purpose is to show the parameters
    in the same order as they appear in DATASET_TEMPLATE. This makes it easier
    to compare the contents of f with the name of a dataset. E.g. for dataset
    name of
    
        CMIP6.ScenarioMIP.CCCma.CanESM5.ssp245.r10i1p1f1.LImon.snm.gn.v20190429   
    
    the corresponding filter dict (i.e. the filter that will grab only this
    dataset) is
    
        f = {
            'mip_era'        : 'CMIP6'
            'activity_drs'   : 'ScenarioMIP'
            'institution_id' : 'CCCma'
            'source_id'      : 'CanESM5'
            'experiment_id'  : 'ssp245'
            'member_id'      : 'r10i1p1f1'
            'table_id'       : 'LImon'
            'variable_id'    : 'snm'
            'grid_label'     : 'gn'
            'version'        : 'v20190429'
        }
    
    and the above display is how fpr(f) shows the dict.
    '''
    l = ['{']
    m = max([len(p) for p in DATASET_TEMPLATE])
    show_apostrophes = not True
    if show_apostrophes: m += 2
    fmt = '%-{}s'.format(m)
    # Display the keys from DATASET_TEMPLATE, if present.
    # Also catch any keys in f that aren't in DATASET_TEMPLATE.
    lk = sorted([k for k in f.keys() if k not in DATASET_TEMPLATE])
    for k,p in enumerate(DATASET_TEMPLATE + lk):
        if p not in f: continue
        ls = [p, f[p]]
        if show_apostrophes:
            for k in range(len(ls)):
                if isinstance(ls[k], (str,unicode)):
                    ls[k] = '\'{}\''.format(ls[k])
        l += ['    {} : {}'.format(fmt % ls[0], ls[1])]
    l += ['}']
    l = [indent + s for s in l]
    if return_str:
        return '\n'.join(l)
    else:    
        print '\n'.join(l)
###############################################################################
def dsd(dataset):
    '''Given a dataset name (str), convert it to a dict containing the
    dataset parameters. The name "dsd" stands for "dataset dict".
    
    E.g. for
        
        dataset = 'CMIP6.ScenarioMIP.CCCma.CanESM5.ssp245.r10i1p1f1.LImon.snm.gn.v20190429'
    
    then dsd(dataset) produces this dict:

        {
            'mip_era'        : 'CMIP6'
            'activity_drs'   : 'ScenarioMIP'
            'institution_id' : 'CCCma'
            'source_id'      : 'CanESM5'
            'experiment_id'  : 'ssp245'
            'member_id'      : 'r10i1p1f1'
            'table_id'       : 'LImon'
            'variable_id'    : 'snm'
            'grid_label'     : 'gn'
            'version'        : 'v20190429'
        }
    '''
    f = {}
    l = dataset.split(SEP_DATASET)
    n = len(l) # number of parameters provided
    n0 = len(DATASET_TEMPLATE) # number of parameters expected
    if n == n0 or n == n0-1:
        # n = n0-1 is allowed because in this case we just assume the dataset
        # version is missing. This is no big deal because the search doesn't
        # use it anyways.
        for k,p in enumerate(DATASET_TEMPLATE[:n]):
            f[p] = l[k]
    return f
###############################################################################
def check_version_format(version):
    '''Return True if version string or version integer (aka the version date)
    is in the correct format.
    
    Example of version string in correct format: "v20190306"
    Example of version integer of correct length:  20190306
    
    Unlike all other parameters that make up dataset names, the version string
    is not part of the CMIP6 controlled vocabulary. Hence there's nothing in
    the data conversion pipeline to stop a version string being "this_version"
    or whatever. But CMIP6 guidance requests that version be a string of the
    form "v" followed by the date given as YYYYMMDD (also referred to above as
    the version integer).
    '''
    ok = False
    if isinstance(version, (str, unicode)):
        s = version.strip('v')
        ok =    len(version) == 9 \
            and version.startswith('v') \
            and version.count('v') == 1 \
            and s.isdigit() \
            and len(s) == 8
        if ok:
            # check the actual value of the integer date is ok
            ok = check_version_format(int(s))
    elif isinstance(version, int):
        s = str(version)
        month = int(s[4:6])
        day =   int(s[6:8])
        ok =    len(s) == 8 \
            and (month >= 1 and month <= 12) \
            and (day   >= 1 and day   <= 31)
    return ok
###############################################################################
def version_num(version):
    '''Return version integer based on input that's either string or integer.
    E.g. "v20190306" --> 20190306
    If input is already a valid version integer then it's returned unchanged.
    '''
    v = None
    if check_version_format(version):
        if isinstance(version, (str, unicode)):
            v = int(version.strip('v'))
        elif isinstance(version, int):
            v = version
    return v
###############################################################################
def version_str(version):
    '''Return version string based on input that's either string or integer.
    E.g. 20190306 --> "v20190306"
    If input is already a valid version string then it's returned unchanged.
    '''
    v = None
    if check_version_format(version):
        if isinstance(version, (str, unicode)):
            v = version
        elif isinstance(version, int):
            v = 'v' + str(version)
    return v
###############################################################################
def dataset_name_rekey(dd):
    '''For a dict keyed by dataset name that does not include the version,
    re-key so that dataset name including the version is used to key the dict
    instead. Intended as an internal function called by others in this module.'''
    ddv = {}
    for dataset in dd:
        for version in dd[dataset]:
            assert check_version_format(version)
            s = SEP_DATASET.join([dataset, version])
            assert s not in ddv
            ddv[s] = dd[dataset][version]
    return ddv
###############################################################################
def select(sel, ddv, l_dataset=None, exclude=None, verbose=True):
    '''
    Select datasets by filtering for variable, experiment, or any other
    parameter.

    Parameters
    ----------
    sel : dict
        Dict giving filtering parameters as strings or lists of strings.
        Or it can also be a list of these kinds of dicts. If it's a list then
        each filter is applied independently and the results collated into a
        single output list.
        (That is, the 2nd filter does not act on the results of the 1st filter;
        each filter is applied to the original list of datasets and the results
        are collected together at the end and output as a single list.)
    ddv : dict
        Dict specifying dataset info, keyed by full dataset name.
    l_dataset : list
        If given, the selection only considers these datasets (rather than
        checking all datasets listed in ddv).
    exclude : dict
        Like 'sel', but criteria are used to exclude variables.

    Returns
    -------
    List of names of the selected datasets.

    How it works
    ------------
    Start with a list of all available datasets and then filter to retain
    only datasets with the specified values of the parameters given in the
    selection dict "sel".

    Since the selecting starts by assuming all available datasets are to be
    published, specifying "sel" as an empty dict, i.e. 
        sel = {}
    will publish all datasets.
    
    Example of a "sel" dict:
        sel = {
            'member_id'     : 'r11i1p1f1',
            'activity_id'   : 'ISMIP6',
            'experiment_id' : 'piControl-withism',
            'grid_label'    : 'gr',
            'institution_id': 'PCMDI',
            'mip_era'       : 'CMIP6',
            'source_id'     : 'PCMDI-test-1-0',
            'table_id'      : 'Amon',
            'variable_id'   : ['tas', 'uas'],
            'version'       : 'v20190105',
         }
    Note that parameters can be specified as lists or strings. 
    There's a further option for specifying lists of combinations of parameters:
        sel = {
            ('table_id', 'variable_id')    : [('Amon','tas'), ('day','tas')],
        }
    This specifies that only the indicated (table,variable) combinations are to
    be retained.
    '''
    if l_dataset is None:
        # Start with all available datasets (all keys of dict "ddv"):
        l_dataset = ddv.keys()
    else:
        # Start with the datasets listed in input variable l_dataset
        assert isinstance(l_dataset, (list, tuple)), 'Input l_dataset must be list'

    if isinstance(sel, dict):
        pass
    elif isinstance(sel, (list,tuple)):
        # If a list of selectors is passed as input, simply loop over this
        # list to call this function for each list entry. Each filter is
        # applied independently and the results collated at the end.
        selectors = sel
        if len(selectors) == 0:
            # Empty selectors list ==> return the input list unaltered
            return l_dataset
        if not all( [isinstance(sel, dict) for sel in selectors] ):
            print('\nList entries of "sel" must all be dicts')
            return []
        l = []
        for sel in selectors:
            l += select(sel, ddv, l_dataset, exclude)
        l = sorted(list(set(l))) # remove any duplicates
        return l
    else:
        print('\nInput variable "sel" must be a selector (dict) or a list of selectors')
        return []

    pp = pprint.PrettyPrinter(indent=4)
    if len(sel) > 0 and verbose:
        print('\nFiltering for these dataset parameters:')
        pp.pprint(sel)
    for p in sel:
        if isinstance(sel[p], (str,unicode)):
            # For convenience, single parameters don't need to be specified
            # as lists. Convert to them to lists here. 
            sel[p] = [sel[p]]

    l = l_dataset
    for p in sel:
        if isinstance(p, tuple):
            # Allowing p to be a tuple allows us to specify combinations of
            # parameters as the filter. This could also be done by building
            # multiple filters (i.e. multiple entries of list lf) but it's
            # simpler just to allow this as an option here. This allows the
            # list lt_priority_vars (or a similar list) to be assigned to
            # any filter. 
            l = [s for s in l if tuple([ddv[s]['params'][pk] for pk in p]) in sel[p]]
            # (I could rewrite the single-parameter filter below to use
            # a length=1 tuple but why bother, it would just make the code
            # harder to read.)
        else:
            # For each criterion given in sel, only retain datasets that 
            # meet the criterion. sel[p] is a list giving the allowed values
            # of parameter p. ddv[s]['params'][p] is the value of that
            # parameter for a given dataset.
            l = [s for s in l if ddv[s]['params'][p] in sel[p]]

    if exclude is not None:
        # Same code as above for selecting, but instead of keeping only the
        # matches we keep only those that don't match.
        sel = exclude
        if len(sel) > 0:
            print('\nExcluding for these dataset parameters:')
            pp.pprint(sel)
        for p in sel:
            if isinstance(sel[p], (str,unicode)):
                sel[p] = [sel[p]]
        for p in sel:
            if isinstance(p, tuple):
                l = [s for s in l if tuple([ddv[s]['params'][pk] for pk in p]) not in sel[p]]
            else:
                l = [s for s in l if ddv[s]['params'][p] not in sel[p]]
    return l
###############################################################################
def aggregate(lp, ddv, l_dataset=None):
    '''
    Create lists of dataset aggregations.
    
    Parameters
    ----------
    lp : list
        List of dataset attributes (parameters) over which to aggregate.
    ddv : dict
        Dict specifying dataset info, keyed by full dataset name.
    l_dataset : list
        If given, the selection only considers these datasets (rather than
        checking all datasets listed in ddv).
    
    Returns
    -------
    Dict containg the dataset aggregations (see below for explanation).
    
    Examples
    --------
    Aggregating over these attributes:
      lp = ['table_id', 'variable_id']
    will produce a list of aggregations that gather together all variables
    (since variables are uniquely defined by the combo of table_id and
    variable_id). The keys of the output dict (da) will have this
    format:
      'CMIP6.CMIP.MRI.MRI-ESM2-0.amip.r1i1p1f1.*.*.gn.v20190308'
    and this entry of da will be a list of all dataset names matching
    this expression (i.e. any table_id,variable_id combination that's
    available for the above model run).
    
    Another example:
      lp = ['institution_id', 'source_id', 'member_id', 'grid_label', 'version']
    produces lists like:
      da['CMIP6.CMIP.*.*.amip.*.Amon.ua.*.*'] =
           ['CMIP6.CMIP.BCC.BCC-CSM2-MR.amip.r1i1p1f1.Amon.ua.gn.v20190121',
            'CMIP6.CMIP.CAMS.CAMS-CSM1-0.amip.r1i1p1f1.Amon.ua.gn.v20190708',
            'CMIP6.CMIP.CAS.FGOALS-f3-L.amip.r1i1p1f1.Amon.ua.gr.v20190422',
            ... ]
    which collects together different models for a given variable & experiment.
    '''
    
    if l_dataset is None:
        # Start with all available datasets (all keys of dict "ddv"):
        l_dataset = ddv.keys()
    else:
        # Start with the datasets listed in input variable l_dataset
        assert isinstance(l_dataset, (list, tuple)), 'Input l_dataset must be list'
    
    # Assign datasets to their aggregations
    da = {}
    for dataset in l_dataset:
        dp = dict(ddv[dataset]['params'])
        dp.update({p : '*' for p in lp})
        s = SEP_DATASET.join([dp[p] for p in DATASET_TEMPLATE])
        if s not in da: da[s] = []
        da[s].append(dataset)
        
    # Check that all datasets are accounted for
    n = len(l_dataset)
    na = 0
    for s in da: na += len(da[s])
    assert na == n, 'Aggregation error: some datasets unaccounted for'
        
    return da
###############################################################################
def ensem(l_dataset, ddv, n=1):
    '''Select a subset of ensemble members for any given experiment.

    This is a specialized selection function, useful to complement select().
    Often we want only a specified number of ensemble members for a given
    model & experiment. E.g. we might only want the first ensemble member
    for all models, so as to create a simple equally-weighted multi-model
    ensemble. However setting member_id='r1i1p1f1' might not accomplish this
    because not all models use this as their first ensemble member. This
    function creates a sorted list of all ensemble members for each
    experiment represented in the list of datasets (l_dataset) and then
    selects a subset of these and returns the list of datasets belonging to
    that subset. E.g. for n=1, it returns all datasets belonging to the first
    ensemble member of that experiment.
    '''
    de = {} # dict to hold any member_id belonging to a given (model,expt)
    dd = {} # dict to hold any dataset name belonging to a given (model,expt)
    for dataset in l_dataset:
        dp = ddv[dataset]['params']
        t = tuple([dp[p] for p in ['source_id', 'experiment_id']])
        # 't' is a tuple indicating the (model,expt). For example:
        #       t = ('CanESM5', 'amip')
        if t not in de: de[t] = set()
        if t not in dd: dd[t] = set()
        de[t].add( dp['member_id'] )
        dd[t].add( dataset )
    for t in de:
        de[t] = sort_member_id(list(de[t]))
    ld = []
    if isinstance(n, (int, float)):
        n = max(int(n), 0)
        for t in de:
            de[t] = de[t][:n]
    else:
        print('Error selecting ensemble members')
        return ld
    # For each (model,experiment) specified by tuple t, de[t] now contains a
    # list of ensemble members to be retained.
    lt = sorted(de.keys(),key=lowercase)
    for t in lt:
        # dd[t] is a list of datasets for this (model,expt).
        # Filter this list to retain only the desired ensemble members.
        ld += [s for s in dd[t] if ddv[s]['params']['member_id'] in de[t]]
    return ld
###############################################################################
def setup_local_archive(local_archive):
    '''Create the local archive dir if it doesn't already exist, and set
    permissions appropriately.'''
    try:
        if not os.path.exists(local_archive):
            os.makedirs(local_archive)
            os.chmod(local_archive, LOCAL_ARCHIVE_PERMISSIONS)
            print('\nCreated directory for local archive: ' + local_archive)
    except:
        print('\nUnable to create directory for local archive: ' + local_archive)
###############################################################################
def check_disk_usage(local_archive, show_msg=True):
    '''Check total space available on disk where the local_archive directory
    resides. Display a message and return the available space.
    '''
    dd = {}
    path = os.path.abspath(local_archive)
    if not os.path.exists(path):
        print('\nLocal archive path does not exist: ' + path)
        return dd
    c = os.statvfs(path)
    block_size = c.f_bsize # size of block, in bytes
    # Get total space and available space (units: bytes)
    total_disk_space = c.f_blocks*block_size # no. of blocks x bytes per block
    available_space  = c.f_bavail*block_size # available blocks x bytes per block
    percent_used     = 100*(total_disk_space - available_space)/float(total_disk_space)
    dd = {
        'path'                  : path,
        'total disk space'      : total_disk_space,
        'available disk space'  : available_space,
        'percent used'          : percent_used,
    }
    if show_msg:
        show_disk_usage(dd)
    return dd
###############################################################################
def show_disk_usage(dd):
    lw = []
    indent = ' '*4
    time_fmt = '%H:%M:%S UTC, %d %b %Y'
    st = datetime.datetime.utcnow().strftime(time_fmt)
    lw.append('Current disk usage for local archive ({0}):'.format(st))
    lws = []
    lws.append( ('Local archive path', dd['path']) )
    lws.append( ('Total disk space', file_size_str( dd['total disk space'] )) )
    lws.append( ('Available disk space', file_size_str( dd['available disk space'] )) )
    lws.append( ('Usage', '{0}%'.format( '%.3g' % dd['percent used'] )) )
    m = max([len(t[0]) for t in lws])
    fmt = '%-'+str(m)+'s'
    lws = ['{0} : {1}'.format(fmt % t[0], t[1]) for t in lws]
    lw += [indent + s for s in lws]
    w = '\n'.join(lw)
    print(w)
###############################################################################
def search(l_filter, json_dir='./.json', verbose=True, keep_params=None):
    '''
    Search ESGF for data, using search parameters given in list l_filter.
    
    Parameters
    ----------
    l_filter : list
        List of dicts, each of wihch gives search parameters as strings or 
        lists of strings.
        
    json_dir : str
    
    verbose : bool
    
    keep_params : list or set
        List of parameters to keep in the "doc" dict returned by the search.
        Default behaviour is to keep all of them, but there are a lot (~50).
        To save on memory the user can choose to retain only a subset.
        However there are a few that are always kept: "instance_id", and
        those in the module variable DATASET_TEMPLATE (11 parameters in total).

    Returns
    -------
    d_found : dict
        Dict containing the search results.
        
    d_info : dict
        Info about how the search was done.
    
    '''
    kp = keep_params
    if kp is not None:
        kp = set(kp)
        kp.update(DATASET_TEMPLATE)
        kp.update(['instance_id'])
    
    # Create list of search filters
    lf = []
    for search_filter in l_filter:
        assert isinstance(search_filter, dict), 'search_filter must be dict'
        if 'dataset' in search_filter:
            # In this case, a dataset or list of datasets has been specified.
            # Convert this here to a dict of parameters that can be processed
            # by the loop over l_filter below.
            lp = ['dataset', 'min_version', 'max_version']
            # Only parameters in lp will be used (if present), others are ignored.
            l = [p for p in search_filter if p not in lp]
            if len(l) > 0:
                print('Since dataset name is given, these other search filter parameters are ignored: ' + ', '.join(l))
            l = search_filter['dataset']
            lp.remove('dataset')
            if isinstance(l, (str,unicode)):
                l = [l]
            assert isinstance(l, list), 'search_filter[\'dataset\'] must be list or string'
            for dataset in l:
                f = dsd(dataset)
                if len(f) == 0:
                    if verbose:
                        print('Invalid dataset name: ' + dataset)
                    continue
                for p in lp:
                    if p in search_filter:
                        f[p] = search_filter[p]
                lf += [f]
        else:
            lf += [search_filter]
    # Ensure each search filter only contains valid parameters
    for f in lf:
        if 'version' in f:
            version = f.pop('version')
            # Cannot search for a specific version, only a range of versions.
            # So if version is given, specify the min,max version to be the
            # specific version requested (i.e. specify a version range that
            # includes only the requested version).
            lp = ['min_version', 'max_version']
            for p in lp:
                if p in f:
                    # if min_version or max_version is already given, don't override it
                    continue
                else:
                    f[p] = version_num(version)
                    # note, min_version & max_version are given as version
                    # numbers rather than version strings.
        lp = f.keys()
        for p in lp:
            if f[p] in ['', None]: f.pop(p)
    l_filter = lf
    del lf
    
    l_search = []
    t_all_searches = time.time()
    n_found_something = 0
    n_found_nothing = 0
    d_info = {'results by filter' : [], 'type' : 'ESGF search'}
    for search_filter in l_filter:
        if len(search_filter) == 0:
            print('Empty search filter - skipping to next search.')
            continue
        if verbose:
            print('\nSearching ESGF for these dataset parameters:')
            fpr(search_filter)

        lp = sorted(search_filter.keys()) # list of parameter names (aka search facets)

        index_node = 'esgf-node.llnl.gov'
        # Note: this corresponds to the "index_node" parameter that will be returned in the
        # search results metadata.

        show_cut_paste_url = True
        if show_cut_paste_url:
            # In case it's useful, show the user a URL that can be cut-paste into the browser URL bar
            # to bring up the same search results in the usual ESGF search view (i.e. the point-click
            # interface). This can be handy to verify the results, or perhaps is a more convenient
            # way to view the metadata, etc.
            #
            # Example of such a URL:
            #   https://esgf-node.llnl.gov/search/cmip6/?&limit=100&source_id=CanESM5&table_id=Amon
            limit = 100 # 100 seems to be the max possible for the browser view. The default is 10.
            lq = ['https://{}/search/cmip6/?limit={}'.format(index_node, limit)]
            for p in lp:
                if isinstance(search_filter[p], (list,tuple)):
                    s = ','.join(search_filter[p])
                else:
                    s = search_filter[p]
                if p == 'member_id':
                    # This is odd, but it seems that the cut-paste URL syntax wants a different
                    # name for this parameter.
                    # (Note, if an incorrect parameter name is passed to the URL, it doesn't produce
                    # an error, it just gets ignored. So failing to pass the desired member_id will
                    # result in all available ensemble members being returned by the search.)
                    p = 'variant_label'
                lq += ['&{}={}'.format(p,s)]
            urlname = ''.join(lq)
            print('To see the same search results in ESGF web search view, cut-paste this URL into your browser:')
            print('    ' + urlname)
            
        limit = 10000
        # The "limit" parameter the maximum number of datasets that can be returned for a given search. It appears
        # that this can be set to any value up to 10000. Since it's possible that a query could return more than
        # 10000 datasets, the loop below will keep repeating the search until all datasets have been found. The
        # "offset" parameter sets how many datasets (out of all available for the given search) to skip before
        # returning a set of results. It's analogous to the "Display N results per page" box on the ESGF website,
        # where you can set N = 10, 20, 50, or 100. Supposing you used N=10, then offset=0 corresponds to the first
        # page of results, offset=10 the second page of results, and so on.
        #
        # I'm not sure if there's any reason to set "limit" to anything other than 10000, but it doesn't really
        # matter what it's set to. The search will continue until all available datasets are returned. "limit"
        # just determines how many queries to the ESGF are required to do that. There might be an optimal value
        # for most efficient searching, but the search is so fast that I'd guess it doesn't matter.

        assert limit > 0 and limit <= 10000, 'Set "limit" to a positive integer not greater than 10000'
        offset = 0
        keep_searching = True
        first_search = True
        n_search = 0
        t_search = time.time()
        while keep_searching:
            # Create URL containing instructions for the search.
            lq  = ['https://{}/esg-search/search/'.format(index_node)]
            lq += ['?offset={}'.format(offset)]
            lq += ['&limit={}'.format(limit)]
            lq += ['&type=Dataset']
            lq += ['&replica=false']

            find_latest_version_only = True
            #if ('min_version' in search_filter) and ('max_version' in search_filter):
            # seems no reason to require both min & max; it works if only one is given
            #if ('min_version' in search_filter) or ('max_version' in search_filter):
            l = ['min_version', 'max_version']
            if any( [p in search_filter for p in l] ):
                find_latest_version_only = False
                # If not restricting the search to just the latest version of any given dataset, the search
                # will return all versions falling within the date ranges given. E.g. 
                #   'min_version'   : 20190301,
                #   'max_version'   : 20190424,
                # It works as <=, >= operators, i.e. min_version=20190306 will return a version v20190306, but
                # not v20190305.
                # Note, min_version & max_version are pass to the ESGF search as numbers (e.g. 20190306),
                # not as version strings (e.g. 'v20190306').
                for p in l:
                    if p not in search_filter: continue
                    search_filter[p] = version_num(search_filter[p]) # ensure version is a number (will convert from str to int if needed)

            if find_latest_version_only:
                lq += ['&latest=true']
            lq += ['&project=CMIP6']
            for p in lp:
                if isinstance(search_filter[p], (list,tuple)):
                    s = ','.join(search_filter[p])
                else:
                    s = search_filter[p]
                lq += ['&{}={}'.format(p,s)]
            lq += ['&format=application%2Fsolr%2Bjson']
            urlname = ''.join(lq)
            # urlname (str) now contains the search URL. If this were cut-pasted into a web browser it would return the
            # json file containing the search results
            
            json_file = 'tmp_search_results.json'
            if not os.path.exists(json_dir): os.makedirs(json_dir)
            filepath = os.path.join(json_dir, json_file)
            
            if verbose:
                print('\nSearching ESGF using search URL:')
                print(' '*2 + urlname)
            urllib.urlretrieve(urlname, filepath)
            with open(filepath, 'r') as f:
                d_json = json.load(f)
            numFound = d_json['response']['numFound']
            num_docs = len(d_json['response']['docs'])
            n_search += 1

            if True: # (13 Aug 2019)
                # Allow for possibility of numFound changing while search is
                # being done. This can happen if data satisfying the search
                # criteria is being published simultaneously while the search
                # is conducted.
                #
                # Previously the code recorded numFound from the first search
                # as numFound0 and checked it matched the value of numFound in
                # all subsequent searches. This was meant to check the
                # consistency of the results. However it doesn't seem to be
                # needed. On each iteration of this loop ("while keep_searching")
                # the whole search is done again but the "offset" and "limit"
                # parameters are used to limit the number of dataset for which
                # info is actually returned (this seems to be a necessity with
                # the ESGF search). So I don't know any reason why numFound
                # can't change while the search is being done. All the results
                # are consolidated below where d_found is constructed anyways. 
                if numFound > 0:
                    n_found_something += 1
                else:
                    n_found_nothing += 1
                    
            else:
                # Previous way (before 13 Aug 2019)
                if first_search:
                    numFound0 = numFound
                    first_search = False
                    if numFound > 0:
                        n_found_something += 1
                    else:
                        n_found_nothing += 1
                else:
                    # numFound should always indicate the total number of available datasets, even if not all of them
                    # are returned by the current search.
                    assert numFound == numFound0

            total_size = sum([d['size'] for d in d_json['response']['docs']])
            number_of_files = sum([d['number_of_files'] for d in d_json['response']['docs']])
            if verbose:
                #print('Search results:')
                expected_number_of_searches = int(np.ceil( numFound/float(limit) ))
                print('Search results ({0} of {1} expected searches):'.format(n_search, expected_number_of_searches))
                if num_docs < numFound:
                    print('  {0} datasets were found using the current search URL (out of {1} total available datasets)'.format(num_docs, numFound))
                else:
                    print('  {0} datasets were found using the current search URL'.format(num_docs))
                print('  total size: {0}, total number of files: {1}'.format(file_size_str(total_size), number_of_files))
            if num_docs == 0: 
                break

            if kp is not None:
                # Keep only specified parameters in each dict belonging to the "docs" list.
                for k,d in enumerate(d_json['response']['docs']):
                    d_json['response']['docs'][k] = dict([ (p,d[p]) for p in kp ])
            
            d_search = {
                'search' : {
                    'filter'    : search_filter,
                    'limit'     : limit,
                    'offset'    : offset,
                    },
                'results' : d_json,
            }
            l_search += [d_search]
            
            if num_docs < limit:
                keep_searching = False
            else:
                assert num_docs == limit # if I understand the ESGF search correctly, this is guaranteed
                offset += num_docs
        
        t_search = time.time() - t_search
        if verbose:
            print('Time taken for search: {0} s'.format('%.3g' % t_search))
            
        d = {
            'filter' : search_filter,
            'number of datasets found' : numFound,
            'time taken for search' : t_search,
        }
        d_info['results by filter'] += [d]
    
    # end of loop over l_filter
    ###########################################################################
    t_all_searches = time.time() - t_all_searches
    if verbose:
        print('\nTime taken for all searches: {0} s'.format('%.3g' % t_all_searches))
        print('Number of searches that found something: {0}'.format(n_found_something))
        print('Number of searches that found nothing: {0}'.format(n_found_nothing))

    time_fmt = '%Y-%m-%d %H:%M:%S'
    search_time_loc = datetime.datetime.now().strftime(time_fmt) # local time
    search_time_utc = datetime.datetime.utcnow().strftime(time_fmt)        
    d_info.update({
        'time for all searches' : t_all_searches,
        'number of searches that found something' : n_found_something,
        'number of searches that found nothing' : n_found_nothing,
        'search finished at (local time zone)' : search_time_loc,
        'search finished at (UTC)' : search_time_utc,
    })
    
    # Organize the search results contained in list l_search into a single dict, d_found, keyed by dataset.
    # The info in l_search depends on the order in which the searches were done and their granularity (including
    # whatever value of the "limit" parameter was used). The purpose of d_found is to organize the same info by
    # dataset, irrespective of how the search was carried out. 
    #
    # E.g. if a given dataset was found by more than one search, then its info appears more than once in l_search.
    # But since a dataset is unique, it will only appear once in d_found. Note that here the dataset strings include
    # the version string, so they must identify unique datasets. Example of a dataset string (aka dataset id) that
    # includes the version string:
    #   'CMIP6.CMIP.NCAR.CESM2.historical.r6i1p1f1.AERmonZ.ua.gn.v20190308'
    # The same dataset without the version string would be:
    #   'CMIP6.CMIP.NCAR.CESM2.historical.r6i1p1f1.AERmonZ.ua.gn'
    # but this might not be a unique dataset if more than one version has been published (i.e. if an earlier version
    # was retracted). To avoid any ambiguity and to distinguish between latest and retracted versions of datasets, here
    # the version string is included in the dataset name.
    #
    # Another dict, d_query, also indexed by dataset (i.e. it has the exact same keys as d_found), remembers info
    # about the searches used to get the data in d_found. Just in case that's useful. It's contained in a separate
    # dict so that it's possible to compare d_found from previous iterations of the script (which might have used
    # different searches to arrive at the same final result) to check if the same info was returned. 
    d_found = {}
    d_query = {}
    for d_search in l_search:
        dr = d_search['results']
        docs = dr['response']['docs']
        for doc in docs:
            # Create dict of parameters defining the dataset
            d_dataset = {}
            for p in DATASET_TEMPLATE:
                if isinstance(doc[p], (list,tuple)):
                    assert len(doc[p]) == 1
                    d_dataset[p] = doc[p][0]
                else:
                    d_dataset[p] = doc[p]
                if p in ['version']: d_dataset[p] = 'v' + d_dataset[p]
            # Use this dict to create the dataset id string
            dataset = SEP_DATASET.join([d_dataset[p] for p in DATASET_TEMPLATE])
            # Validate the dataset id string
            assert dataset == doc['instance_id']
            
            df = {
                'doc'       : doc,
                'params'    : d_dataset,
            }
            if dataset not in d_found:
                # Store all info (metadata) returned by the search for this dataset.
                d_found[dataset] = df
                d_query[dataset] = [d_search['search']]
            else:
                # If dataset was already found as part of another search, verify that the same info about the data is given.
                assert d_found[dataset] == df
                # Then append the search filters to the list of filters, to indicate that the data appeared in more than
                # one search.
                d_query[dataset] += [d_search['search']]
            del df  

    n_found = len(d_found)
    d_info.update({
        'total number of datasets found' : n_found,
    })
    print('Total number of datasets found: {0}'.format(n_found))

    return d_found, d_info
###############################################################################
def summarize(d_found, d_info, axes, tables, summary_dir='./summary', \
    show_results_by_filter=False, return_datasets=False):
    '''
    Display a summary of ESGF search results or local archive contents.
    
    Parameters
    ----------
    d_found : dict
        Dict containing search results or local inventory.
        Search results are output from this module's search() function.
        Local inventory are output from this module's inventory() function.
    
    d_info : dict
        Info about how the search/inventory was done.
    
    axes : dict
        Dict with "row" and "column" entries that define the axes of the
        table summary. See search.py for examples.

    tables : list
        List of tables to show.
        See search.py for examples.
    
    summary_dir : str
        Location in which to write file(s) containing table summaries.
        If None, don't write any files.
    
    show_results_by_filter : bool
        Option to show info on results for each search filter used.
    
    return_datasets : bool
        Option to just return dict containing the datasets classified
        according to the specified axes. Table summaries are not
        displayed or written to file.
    
    '''
    if len(d_found) == 0: return
    if summary_dir is not None:
        # Write file(s) containing the summary
        assert isinstance(summary_dir, (str, unicode))
        if not os.path.exists(summary_dir): os.makedirs(summary_dir)
        time_fmt = '%Y-%m-%d'
        summary_date = datetime.datetime.now().strftime(time_fmt)
        #stem = 'esgf_search_{0}'.format(summary_date)
        if   d_info['type'] in ['ESGF search']:
            filename0 = 'esgf_search'
        elif d_info['type'] in ['local inventory']:
            filename0 = 'local_archive'
        else:
            assert False, 'Unknown type of info: ' + d_info['type']
        stem = '{0}_{1}'.format(filename0, summary_date)
        l = sorted([s for s in os.listdir(summary_dir) if s.startswith(stem) and s.endswith('.txt')])
        if len(l) > 0:
            latest_summary_file = l[-1]
            latest_edition_str = os.path.splitext( latest_summary_file.rpartition(stem)[-1] )[0].strip('_')
            m = int(latest_edition_str)
        else:
            m = 0
        summary_edition = str('%.3i' % (m+1))
        summary_file = stem + '_{0}'.format(summary_edition)
        # summary_file doesn't include the file extension (e.g. ".txt") - this
        # will be added below, depending on the type(s) of summary file written.
    
    l_dataset = sorted(d_found.keys(), key=lowercase)
  
    indent = ' '*2
    d_axes = {}
    for ax in axes:
        lp = axes[ax] # list of parameter names
        if not isinstance(lp, (list, tuple)): lp = list(lp)
        for p in lp: assert p in DATASET_TEMPLATE, 'unknown parameter: ' + p
        p_template = [p for p in DATASET_TEMPLATE if p in lp]
        d_axes[ax] = {}
        for dataset in l_dataset:
            if 'retracted' in d_found[dataset]['doc']:
                if d_found[dataset]['doc']['retracted']:
                    # Exclude retracted datasets from the summary
                    continue
            d = {}
            for p in lp:
                d[p] = d_found[dataset]['params'][p]
            v = SEP_DATASET.join([d[p] for p in p_template])
            v = str(v)
            if v not in d_axes[ax]:
                d_axes[ax][v] = set()
            d_axes[ax][v].add(dataset)
    
    axes_values = {}
    for ax in axes:
        if 'member_id' in axes[ax]:
            axes_values[ax] = sort_datasets(d_axes[ax].keys(), member_index=axes[ax].index('member_id'))
        else:
            axes_values[ax] = sorted(d_axes[ax].keys(), key=lowercase)

    if   d_info['type'] in ['ESGF search']:
        print('\nSummarizing search results:')
    elif d_info['type'] in ['local inventory']:
        print('\nSummarizing local archive inventory:')

    if not True:
        for ax in axes_values:
            print(indent + 'no. of ' + ax + 's: ' + str(len(axes_values[ax])))
    
    # Create a general table that groups datasets together according to the specified axes.
    # Each table entry will be the set of dataset names with the parameter values indicated by the row/column labels.
    # Can then use this info to create tables showing specific info, e.g. no. of datasets for each entry, total data
    # size, etc.
    # A dict is used to represent the table.
    dataset_table = {}
    for vr in axes_values['row']:
        for vc in axes_values['column']:
            dataset_table[(vr,vc)] = d_axes['row'][vr].intersection(d_axes['column'][vc])

    if return_datasets:
        # Don't display tables. Instead just return dicts containing the
        # classification of datasets that would have been used in the tables.
        return dataset_table, axes_values
            
    l_table = tables
    # l_table specifies what types of tables to show, i.e. what quantity is displayed as the table entries.
    #
    # Possibly to-do: if I were to load info from the CMIP6 json tables (or dreqPy) then the info wouldn't be limited to just
    # what's in the metadata. E.g. for a given CMOR variable could show the dimensions, etc.
    
    ws = ''
    d_table = {}
    for table_type in l_table:

        tab = copy.deepcopy(dataset_table)
        rows = list(axes_values['row'])
        columns = list(axes_values['column'])
        shp = (len(rows), len(columns))
        
        atab = np.zeros(shp)
        atab.fill(np.nan)
        
        formatter = None
        for kr,vr in enumerate(rows):
            for kc,vc in enumerate(columns):
            
                a = ''
                if table_type == 'no. of datasets':
                    a = len(tab[(vr,vc)])
                    atab[kr,kc] = a
                    formatter = '{:.0f}'.format
                    a = formatter(a)
                elif table_type == 'total size of datasets':
                    total_size = 0
                    for dataset in tab[(vr,vc)]:
                        total_size += d_found[dataset]['doc']['size']
                    atab[kr,kc] = total_size
                    formatter = file_size_str
                    if total_size == 0:
                        a = '-'
                    else:
                        a = formatter(total_size)
                elif table_type in ['unique values of ' + p for p in DATASET_TEMPLATE]:
                    lv = []
                    p = table_type.split()[-1]
                    for dataset in tab[(vr,vc)]:
                        v = d_found[dataset]['params'][p]
                        if v not in lv: 
                            lv += [str(v)]
                    if p == 'member_id':
                        lv = sort_member_id(lv)
                    else:
                        lv = sorted(lv, key=lowercase)
                    a = ', '.join(lv)
                elif table_type in ['estimated no. of years']:
                    # Estimate the number of years, based on the given start/stop times.
                    # We don't know the calendar used by the model (it's not in the metadata provided by the search).
                    # So this just assumes the usual one (365 days per year, leap years) by using datetime objects.
                    total_years = 0
                    # start,stop times should be given as strings in the format: '2014-12-15T12:00:00Z'
                    fmt = TIME_FMT_DOC
                    
                    for dataset in tab[(vr,vc)]:
                        if 'datetime_start' not in d_found[dataset]['doc']: continue
                        if 'datetime_stop' not in d_found[dataset]['doc']: continue
                        date_start = d_found[dataset]['doc']['datetime_start']
                        date_stop  = d_found[dataset]['doc']['datetime_stop']
                        if date_start is None or date_stop is None: continue
                        dt_start = datetime.datetime.strptime(date_start, fmt)
                        dt_stop  = datetime.datetime.strptime(date_stop, fmt)
                        elapsed_days = (dt_stop - dt_start).days
                        total_years += elapsed_days/365.
                        
                    atab[kr,kc] = total_years
                    formatter = '{:.1f}'.format
                    if total_years == 0:
                        a = '-'
                    else:
                        a = formatter(total_years)
                elif table_type == 'no. of ensemble members':
                    p = 'member_id'
                    lv = []
                    for dataset in tab[(vr,vc)]:
                        v = d_found[dataset]['params'][p]
                        if v not in lv:
                            lv += [str(v)]
                    a = len(lv)
                    atab[kr,kc] = a
                    formatter = '{:.0f}'.format
                    a = formatter(a)
                else:
                    assert False, 'unknown table type: ' + table_type

                tab[(vr,vc)] = a
        
        ncol = len(columns)
        nrow = len(rows)

        show_totals = True
        if np.any(np.isnan(atab)):
            show_totals = False
        if show_totals:
            col_sum = atab.sum(1) # for each row, this is the sum over all columns
            row_sum = atab.sum(0) # for each column, this is the sum over all rows
            all_sum = atab.sum()
            if formatter is not None:
                col_sum = np.array([ formatter(m) for m in col_sum ])
                row_sum = np.array([ formatter(m) for m in row_sum ])
                all_sum = formatter(all_sum)
            s = 'TOTAL:'
            lt = [(s, c) for c in columns]
            rows += [s]
            for k,t in enumerate(lt):
                tab[t] = row_sum[k]
            col_sum = np.concatenate((col_sum, np.array([all_sum])))
            s = 'TOTAL'
            lt = [(r, s) for r in rows]
            columns += [s]
            for k,t in enumerate(lt):
                tab[t] = col_sum[k]

        show_number_of_entries = True
        if show_number_of_entries:
            b = np.where( atab != 0 )
            a = np.zeros(atab.shape, int)
            a[b] = 1
            col_sum = a.sum(1) # for each row, this is the sum over all columns
            row_sum = a.sum(0) # for each column, this is the sum over all rows
            all_sum = a.sum()
            s = 'entries:'
            lt = [(s, c) for c in columns[:ncol]]
            rows += [s]
            for k,t in enumerate(lt):
                tab[t] = row_sum[k]
            col_sum = np.concatenate((col_sum, np.array([all_sum])))
            s = 'entries'
            lt = [(r, s) for r in rows[:nrow]]
            columns += [s]
            for k,t in enumerate(lt):
                tab[t] = col_sum[k]

        # Display the table   
        lw = []
        lw.append( '\n' + indent + table_type.capitalize() + ':' )
        lw.append(        indent + '-'*(len(table_type) + 1) )
        show_row_count = True
        row = []
        col0 = ['']
        col0 += list(columns)
        
        if show_row_count: col0 = [''] + col0
        row += [col0]

        for k,vr in enumerate(rows):
            #col = [vr] + [tab[(vr,vc)] for vc in columns]
            col = [vr]
            for vc in columns:
                t = (vr,vc)
                if t in tab:
                    col.append(tab[t])
                else:
                    col.append('')
            if show_row_count: 
                #if col[0].lower().startswith('total'): s = ''
                no_show = ['total', 'entries']
                if any([col[0].lower().startswith(s) for s in no_show]): s = ''
                else: s = str(k+1) + '.'
                col = [s] + col
            row += [col]
        underline = [0]
        emptyline = []
        if show_totals: 
            if show_number_of_entries:
                emptyline += [len(row)-3]
            else:
                emptyline += [len(row)-2]
        lw.append( column_fmt(row, underline=underline, emptyline=emptyline, indent=indent, gap=' '*2, max_len=35) )
        w = '\n'.join(lw)
        print(w)
        ws += w

        d_table[table_type] = row

    write_txt = True
    write_xlsx = True
    write_csv = not True

    if summary_dir is None:
        # Don't write summary info to any kind of output file.
        write_txt = not True
        write_xlsx = not True
        write_csv = not True
    
    if write_txt:
        lw = []
        if d_info['type'] in ['ESGF search']:
            lw.append( 'Summary of search results:\n' )
            search_time_loc = d_info['search finished at (local time zone)']
            search_time_utc = d_info['search finished at (UTC)']
            lw.append( 'Search finished at: {0} ({1} UTC)'.format(search_time_loc, search_time_utc) )
            t = d_info['time for all searches']
            fmt = '%.4g'
            #lw.append( 'Search took {0} s ({1} min; {2} hr)'.format(fmt % t, fmt % float(t/60.), fmt % float(t/3600.)) )
            lw.append( 'Search took {0} s ({1} min)'.format(fmt % t, fmt % float(t/60.)) )
        elif d_info['type'] in ['local inventory']:
            lw.append( 'Summary of local archive inventory:\n' )
            inventory_time_loc = d_info['inventory finished at (local time zone)']
            inventory_time_utc = d_info['inventory finished at (UTC)']
            lw.append( 'Local inventory finished at: {0} ({1} UTC)'.format(inventory_time_loc, inventory_time_utc) )
            t = d_info['time taken to do local inventory']
            fmt = '%.4g'
            lw.append( 'Local inventory took {0} s ({1} min)'.format(fmt % t, fmt % float(t/60.)) )            
        lw.append( 'Total number of datasets found: {0}'.format(d_info['total number of datasets found']) )
        lw.append( '(Note: 1 "dataset" = 1 output field from 1 model run)' )
        if d_info['type'] in ['ESGF search']:
            lw.append( 'Number of search filters used: {0}'.format(len(d_info['results by filter'])))
            n_found_something = d_info['number of searches that found something']
            n_found_nothing = d_info['number of searches that found nothing']
            lw.append( 'Number of search filters that found something: {0}'.format(n_found_something) )
            lw.append( 'Number of search filters that found nothing: {0}'.format(n_found_nothing) )
            if show_results_by_filter:
                hor_line = '-'*80
                lw.append( hor_line )
                for k,d in enumerate(d_info['results by filter']):
                    lw.append( 'Filter {0}:'.format(k+1) )
                    lw.append( 'filter = ' + fpr(d['filter'], indent, return_str=True).strip() )
                    t = d['time taken for search']
                    lw.append( 'Search took {0} s ({1} min)'.format(fmt % t, fmt % float(t/60.)) )
                    lw.append( 'Number of datasets found: {0}'.format(d['number of datasets found']) )
                    lw.append( hor_line )
        for k in range(1,len(lw)): lw[k] = indent + lw[k]
        w = '\n'.join(lw)
        ws = '\n'.join([w, ws])
    
        # Write plain text file containing the summary.
        filename = os.path.extsep.join([summary_file, 'txt'])
        filepath = os.path.join(summary_dir, filename)
        with open(filepath, 'w') as f:
            f.write(ws)
            os.chmod(filepath, 0644)
            print('Wrote summary file: ' + filepath)

    if write_xlsx:
        try: xp
        except: 
            #print('Skipping xlsx write because openpyxl module not available.')
            print('Skipping xlsx write because openpyxl module not available. Writing csv files instead.')
            write_xlsx = False
            write_csv = True

    if write_xlsx:
        # Write spreadsheet with each summary table as a separate sheet.
        fontname = 'Calibri'
        show_totals_in_bold = False
        show_alternating_grey_rows = False
        d_color = {
        # https://htmlcolorcodes.com/
            'white'      : 'FFFFFF',
            'light grey' : 'D5D5D5',
        }
        wb = xp.Workbook()
        wb.remove(wb.worksheets[0])
        for table_type in l_table:
            sheet = table_type
            ws = wb.create_sheet(sheet)
            ws.title = sheet
            #row = d_table[table_type]
            row = copy.deepcopy( d_table[table_type] )
            l_axis_values = axes_values['row'] + axes_values['column']
            for k, col in enumerate(row):
                current_row = k + 1
                row_expand = 0
                for m,s in enumerate(col):
                    if s in l_axis_values:
                        if '.' not in s: continue
                        col[m] = s.replace('.','\n')
                        n = col[m].count('\n')
                        if n > 0:
                            # expand cell vertically so that all text is visible
                            row_expand = n*25
                            #ws.row_dimensions[current_row].height = n*25
                ws.append(col)
                if row_expand > 0:
                    ws.row_dimensions[current_row].height = row_expand
                if show_totals_in_bold:
                    for m,s in enumerate(col):
                        # indicate headings for totals in bold
                        if s in ['TOTAL', 'TOTAL:']:
                            cell = xp.utils.get_column_letter(m+1) + str( current_row )
                            c = ws[cell]
                            c.font = xp.styles.Font(size=14, bold=True, name=fontname)
                if show_alternating_grey_rows:
                    if np.mod(current_row,2) == 1: color = d_color['white']
                    else: color = d_color['light grey']
                    for m,s in enumerate(col):
                        cell = xp.utils.get_column_letter(m+1) + str( current_row )
                        c = ws[cell]
                        c.fill = xp.styles.PatternFill(start_color=color, end_color=color, fill_type='solid')
            # Estimate column widths
            col0 = row[0]
            ncol = len(col0)
            assert all([len(col) == ncol for col in row])
            lcol = zip(*row)
            #col_max = [ max([len(s) for s in col]) for col in lcol ]
            col_max = []
            for col in lcol:
                m = 0
                for s in col:
                    if '\n' in s:
                        l = s.split('\n')
                        m = max( m, max([len(s1) for s1 in l]) )
                    else:
                        m = max( m, len(s) )
                col_max.append(m)
            assert len(col_max) == ncol
            # Set column width for better readability
            for k in range(ncol):
                s = xp.utils.get_column_letter(k+1)
                col_width = min( col_max[k] + 5, 30 )
                ws.column_dimensions[s].width = col_width
        filename = os.path.extsep.join([summary_file, 'xlsx'])
        filepath = os.path.join(summary_dir, filename)
        wb.save(filename = filepath)
        os.chmod(filepath, 0644)
        print('Wrote xlsx file: ' + filepath)

    if write_csv:
        # Write csv files containing each summary table.
        # Probably less useful than xlsx output, unless the openpyxl module
        # isn't available.
        for table_type in l_table:
            row = d_table[table_type]

            s = table_type
            #s = s.replace('.','')
            s = s.replace(' ','_')
            s = s.replace('no.','number')
            #s = '_'.join([summary_file, s])
            filename = os.path.extsep.join([s, 'csv'])
            csv_dir = os.path.join(summary_dir, summary_file + '_csv')
            if not os.path.exists(csv_dir): os.makedirs(csv_dir)
            filepath = os.path.join(csv_dir, filename)
            with open(filepath, 'wb') as csvfile:
                csvwriter = csv.writer(csvfile, delimiter=',')
                for col in row:
                    csvwriter.writerow(col)
                os.chmod(filepath, 0644)
                print('Wrote csv file: ' + filepath)

###############################################################################
def t2dt(time_str):
    '''
    Convert a time string into a datetime object and also a string.
    Purpose is to convert time strings as found in netcdf file names, e.g. in
        tas_Amon_CanESM5_historical_r5i1p2f1_gn_185001-201412.nc
    the time string is '185001-201412'.
    
    Parameters
    ----------
    time_str : str
        Time string, e.g. '185001', '19500301'
    
    Returns
    -------
    dt : datetime.datetime object 
        Object representing the time indicated by the input time string
        
    st : string
        String in format used by ESGF 'doc' dict representing the time
        indicated by the input time string
    '''
    assert isinstance(time_str, (str, unicode)), \
        'input must be a time string in a recognized format (e.g. in YYYYMMDD format: \'20150101\')'

    # List of recognized formats:
    date_formats = ['YYYY', 'YYYYMM', 'YYYYMMDD', 'YYYYMMDDHH']

    # Determine the format of the input string by matching its length to the
    # length of one of the recognized formats
    l = [s for s in date_formats if len(s) == len(time_str)]
    assert len(l) == 1, 'unknown time format for time string: ' + time_str    
    time_input = l[0]
    
    l_date_part = chksme(time_input)
    d_ind = {}
    #fmt_date = ''
    d_fmt = {}
    for sp in l_date_part:
        m0 = time_input.index(sp)
        m1 = time_input.rindex(sp) + 1
        nm = m1-m0
        assert time_input[m0:m1] == sp*nm
        d_ind[sp] = (m0, m1)
        #fmt_date += '%.' + str(nm) + 'i'
        d_fmt[sp] = '%.' + str(nm) + 'i'
    d_t = {}
    #l = [str(m) for m in tv]
    for sp in l_date_part:
        m0, m1 = tuple([ d_ind[sp][0], d_ind[sp][1] ])
        #d_t[sp] = np.array( [int(s[m0:m1]) for s in l ] )
        d_t[sp] = int(time_str[m0:m1])
        
    validate_time_ranges = True
    if validate_time_ranges:
        # Check that valid values were found.
        mpy = 12 # max months per year
        dpm = 31 # max days per month
        hpd = 24 # max hours per day

        # testing:        
        yr1 = 1e6
        yr0 = -yr1
        # (could get valid years from the experiments table)
        
        if 'Y' in d_t: # years
            assert d_t['Y'] >= yr0
            assert d_t['Y'] <= yr1
        if 'M' in d_t: # months
            assert d_t['M'] >= 1
            assert d_t['M'] <= mpy
        if 'D' in d_t: # days
            assert d_t['D'] >= 1
            assert d_t['D'] <= dpm
        if 'H' in d_t: # hours
            assert d_t['H'] >= 0
            assert d_t['H'] <= hpd-1
    
    # Recreate the time string to ensure it was correctly parsed
    s = ''.join([ str(d_fmt[sp] % d_t[sp]) for sp in l_date_part ])
    assert s == time_str, 'Time string {0} incorrectly parsed to {1}'.format(time_str, s)
    
    # Create datetime object corresponding to time_str
    dfps(d_t, {'M' : 1, 'D' : 16, 'H': 12}) # defaults for month,day,hour if not provided
    dt = datetime.datetime(*[d_t[sp] for sp in ['Y','M','D','H']])
    
    # Create new string in the format used by 'doc' dicts, corresponding
    # to the datetime object
    year_fix = True
    fmt = TIME_FMT_DOC

    if year_fix:
        # Seems datetime.datetime.strftime() cannot handle years before 1900!
        # (At least in the version I'm using as of 7 Aug 2019.)
        # year_fix=True does an ad-hoc fix for this. Probably can remove this
        # in later versions.
        y0 = 1900
        sep = '-'
        assert fmt.startswith('%Y' + sep)
        if dt.year < y0:
            dt = dt.replace(year=y0)
            replace_year = True
        else:
            replace_year = False
            
    st = datetime.datetime.strftime(dt, fmt)

    if year_fix:
        if replace_year:
            st = sep.join([str('%.4i' % d_t['Y']), st.partition(sep)[-1]])
            dt = dt.replace(year=d_t['Y'])
 
    return dt, st
 
###############################################################################
def inventory(l_path, l_sel=None):
    '''
    Scan through directories on the server to gather information about them.

    The paths in "l_path" MUST be top-level dirs. They cannot just be any
    arbitrary dir in the tree. The reason for this is that info about each
    dataset is extracted from the components of the dir path. Example: suppose
    the netcdf files of a dataset are in this directory:
      /esgA/data/AR6/CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Amon/tas/gn/v20190108
    Then the valid top-level dir to include in l_path is:
      /esgA/data/AR6
    In other words, the directory tree that this function expects to find
    is structured as:
      mip_era/
        activity_id/
          institution_id/
            source_id/
              experiment_id/
                member_id/
                  table_id/
                    variable_id/
                      grid_label/
                        version/
    
    Parameters
    ----------
    l_path : list of strings
        List of paths to search for data.
    l_sel : (optional) list of dicts
        Selectors to narrow down the search. This can help speed up the seach
        when the local archive is large. Passing an empty list or empty dict
        means that no filtering is done (i.e. all directories are searched).
    
    Returns
    -------
    ddv : dict
        Gives info on datasets, keyed by dataset name.
        The key is the full dataset name, including the version. For example:
        
            ddv['CMIP6.ScenarioMIP.CCCma.CanESM5.ssp585.r9i1p1f1.SImon.siconc.gn.v20190326']
                
    '''
    if isinstance(l_path, (str, unicode)):
        # Allow a single path to be passed as input, rather than a list of paths.
        l_path = [l_path]
    assert isinstance(l_path, list), 'Input l_path must be a list of top-level paths'
    assert all([isinstance(path, (str,unicode)) for path in l_path]), 'Paths must be string'
    if (l_sel is None) or (l_sel == []): l_sel = {}
    if isinstance(l_sel, dict):
        # Allow a single selector dict to be passed as input, rather than a list of dicts.
        l_sel = [l_sel]
    assert isinstance(l_sel, list), 'Input l_sel must be a list of dicts'
    assert all([isinstance(sel, dict) for sel in l_sel]), 'l_sel entries must be dicts'
    l_sel = copy.deepcopy(l_sel)
    for sel in l_sel:
        for p in sel:
            if isinstance(sel[p], (str, unicode)):
                sel[p] = [sel[p]]
            assert isinstance(sel[p], (list, tuple))
    
    ext_filename = ['.nc'] # list of allowed file extensions for data files

    # "output_path_template" tells this function how to extract dataset
    # parameters from the directory name. It gives the components of
    # directory tree in which datasets reside.
    #s = '<mip_era><activity_id><institution_id><source_id><experiment_id><member_id><table><variable_id><grid_label><version>'
    # 7aug.19: update to make these parameter names exactly consistent with the parameter names used when searching ESGF
    #s = '<mip_era><activity_drs><institution_id><source_id><experiment_id><member_id><table_id><variable_id><grid_label><version>'
    #output_path_template = tuple(s.strip('<').strip('>').split('><'))
    # 17sep.19: define output_path_template based on the module-level variable to ensure
    # consistency (should not have it defined in more than one place).
    output_path_template = tuple(OUTPUT_PATH_TEMPLATE[1:])

    #s = '<variable_id><table><source_id><experiment_id><member_id><grid_label>'
    # 7aug.19: update to make these parameter names exactly consistent with the parameter names used when searching ESGF
    #s = '<variable_id><table_id><source_id><experiment_id><member_id><grid_label>'
    #output_file_template = tuple(s.strip('<').strip('>').split('><'))
    # 17sep.19: define this as a module-level variable

    dd = {}
    n_op = len(output_path_template)
    #dataset_template = [s for s in output_path_template if s not in ['version']]
    # 17sep.19: define dataset_template based on the module-level variable to ensure
    # consistency (should not have it defined in more than one place).
    dataset_template = [s for s in DATASET_TEMPLATE if s not in ['version']]
    
    size_total = 0
    print('\nScanning data dirs...')
    t_total = time.time()
    
    for sel in l_sel:
    
        if len(sel) > 0:
            print('\nChecking local inventory for these dataset parameters:')
            fpr(sel)
    
        for path in l_path:
            if not os.path.exists(path):
                print('Path does not exist: ' + path)
                continue
            size_in_path = 0
            t_path = time.time()
            datasets_in_path = []
            
            #for tw in os.walk(path):
            #for tw in os.walk(path, topdown=True): # topdown=True should be the default, but let's be explicit
            walk = os.walk
            
            if not True:
                # 20sep.19: scandir.walk() is supposed to be faster than
                # os.walk().
                try:
                    import scandir
                    walk = scandir.walk
                except:
                    pass
            
            for tw in walk(path, topdown=True): # topdown=True should be the default, but let's be explicit
            

                if True:
                    # new way (4sep.19), allowing selectors
                    
                    dirpath, dirnames, filenames = tw
                    relpath = os.path.relpath(dirpath,path)
                    if relpath in ['.']: continue
                    tp = relpath.strip(os.path.sep).split(os.path.sep)
                    d = {}
                    for k,v in enumerate(tp): 
                        # Create dict of dataset parameters
                        d[output_path_template[k]] = v

                    for p in sel:
                        if p not in d: continue
                        if d[p] not in sel[p]:
                            # This parameter value (aka search facet) is not
                            # wanted. So we can skip all the dirs in this folder.
                            #print('Skipping parameter: {}={}'.format(p, d[p]))
                            for p in list(dirnames):
                                dirnames.remove(p)
                    
                else:
                    # previous way (before allowing selectors)
                    
                    dirpath = tw[0]
                    relpath = os.path.relpath(dirpath,path)

                    tp = relpath.strip(os.path.sep).split(os.path.sep)
                    #if len(tp) != n_op: 
                    #    continue
                    #d = {output_path_template[k] : v for k,v in enumerate(tp)}
                    d = {}
                    for k,v in enumerate(tp): 
                        d[output_path_template[k]] = v

                        
                if len(tp) != n_op: 
                    continue
                
                td = tuple([d[s] for s in dataset_template])
                #  tp : tuple of parameter values in the output dir path
                #  td : tuple of parameter values in the dataset name
                #  d : dict giving values of the parameters
                dataset = SEP_DATASET.join(td)
                version = d['version']
                assert check_version_format(version)

                # Determine list of filenames            
                filename0 = SEP_FILENAME.join([d[s] for s in OUTPUT_FILE_TEMPLATE])
                files = tw[2]
                files = [s for s in files if s.startswith(filename0)]
                files = [s for s in files if os.path.splitext(s)[-1] in ext_filename]
                files = sorted(files)
                dirsize = sum([os.path.getsize(os.path.join(dirpath,filename)) \
                               for filename in files])
                if len(files) == 0 or dirsize == 0:
                    # Dir is empty, so skip it.
                    continue

                # Determine start,stop times from the time strings in the filenames
                lt = [os.path.splitext(filename)[0].split(SEP_FILENAME)[-1] for filename in files]
                # lt is now list of time string (e.g. '185001-201412') for all filenames in list 'files'
                lt = [tuple(s.split('-')) for s in lt] # convert the strings to tuples giving pairs of times
                if all([len(t) == 2 for t in lt]):
                    t0 = lt[0][0]
                    t1 = lt[-1][1]
                    dt0, st0 = t2dt(t0)
                    dt1, st1 = t2dt(t1)
                else:
                    # Could not determine time string. This can happen for fx (fixed) variables
                    # (or it could be an error).
                    assert d['table_id'] in ['fx', 'Ofx', 'Efx', 'AERfx', 'IfxAnt', 'IfxGre'],\
                        'Error finding start,stop times for dataset in dir: {}'.format(dirpath)
                    st0 = None
                    st1 = None

                if dataset not in dd: 
                    dd[dataset] = {}
                if version not in dd[dataset]:
                    
                    if True:
                        # 6aug.19 reorganizing this info so that d_found & d_local can be more similar
                        # General info about the dataset is in a dict called 'doc' since the equivalent info
                        # in d_found is in dict 'doc'. Fields that give the same info (e.g. 'size', 
                        # 'datetime_start') should have the same names as used in the d_found version
                        # of 'doc'. However note that many fields are unique to one or the other (e.g.
                        # in the d_local the paths indicating the location of the dataset on disk). 
                        dd[dataset][version] = {
                            'dataset_name'  : dataset,  # name of the dataset without the version string
                            'params'        : d,        # dict for parameters that make up the dataset name (source_id, etc)
                            'doc'           : {         # other info about the dataset
                                'size'          : dirsize,
                                'size_str'      : file_size_str(dirsize),
                                'files'         : files,
                                'number_of_files' : len(files),
                                'datetime_start' : st0,
                                'datetime_stop'  : st1,
                                'rootdir'       : path,
                                'relpath'       : relpath,
                                'abspath'       : dirpath,
                            }
                        }
                        
                    else:
                        # old way (before 6aug.19), which was simply copied from my ESGF publishing script
                        dd[dataset][version] = {
                            'dataset'   : dataset,
                            'params'    : d,
                            'files'     : files,
                            'size'      : dirsize,
                            'size_str'  : file_size_str(dirsize),
                            'rootdir'   : path,
                            'relpath'   : relpath,
                            'abspath'   : dirpath,
                        }
                    
                    size_in_path += dirsize
                    datasets_in_path += [dataset]
                else:
                    # If this version of this dataset has already been found, continue
                    print('Redundant dir: ' + dirpath)
            size_total += size_in_path
            t_path = time.time() - t_path
            fmt = '%.1f'
            print('\nPath: ' + path \
                + '\n  No. of datasets found: ' + str(len(datasets_in_path))
                + '\n  Total size for this path: ' + file_size_str(size_in_path)
                #+ '\n  Time taken to search this path: ' + str('%.2g' % t_path) + ' s')
                + '\n  Time taken to search this path: {} s ({} min)'.format(fmt % t_path, fmt % float(t_path/60.)))

    t_total = time.time() - t_total
    fmt = '%.1f'
    print('\nTotal size of all datasets in all paths: ' + file_size_str(size_total) \
        #+ '\nTime taken to search all paths: ' + str('%.2g' % t_total) + ' s')
        + '\nTime taken to search all paths: {} s ({} min)'.format(fmt % t_total, fmt % float(t_total/60.)))

    if not True:
        # print contents of dd (for testing)
        print
        print len(dd)
        for s in dd.keys(): 
            print s
            continue
            
            d_dataset = dd[s]
            version = d_dataset.keys()[0]
            if len(d_dataset[version]['files']) > 1:
                print('\ndataset: ' + dataset + '\nversion: ' + version)
                print( d_dataset[version] )
                stop

    # Create a dict of links to the same datasets as in "dd", but refer to
    # them by their dataset+version name. This is useful because a single loop
    # over this dict (called "ddv") loops over all datasets. 
    #
    # The version created above, dd, is a bit tidier in that multiple versions
    # of the same dataset are contained under a single dict entry. But usually
    # we don't have a whole lot of versions of the same dataset, and this form
    # hasn't been as useful when writing the code. I don't want to rewrite the
    # code above to use the ddv instead of dd form, but the ddv will be passed
    # as output and we shouldn't need the dd form again.
    ddv = dataset_name_rekey(dd)

    time_fmt = '%Y-%m-%d %H:%M:%S'
    inventory_time_loc = datetime.datetime.now().strftime(time_fmt) # local time
    inventory_time_utc = datetime.datetime.utcnow().strftime(time_fmt)        

    d_info = {
        'type' : 'local inventory',
        'paths' : list(l_path),
        'selectors' : l_sel,
        'time taken to do local inventory' : t_total,
        'inventory finished at (local time zone)' : inventory_time_loc,
        'inventory finished at (UTC)' : inventory_time_utc,
        'total number of datasets found' : len(ddv),
        'total size of all datasets found' : size_total,
    }

    return ddv, d_info
###############################################################################
def dict_retain(d, keep):
    '''
    Return dict with only selected keys retained from input dict "d".
    "keep" is a list indicating what to retain.
    Example:
        d = {'a' : 1, 'b' : 2, 'c' : 3,
             'stuff' : {'d' : 4, 'e' : 5, 'f' : 6}}
        keep = ['a', 'b', {'stuff' : ['e', 'f']}]
    returns the dict
        d = {'a' : 1, 'b' : 2,
             'stuff' : {'e' : 5, 'f' : 6}}
    Copies of any of the dict contents are not explicitly made.
    '''
    assert isinstance(d, dict)
    e = d.__class__()
    for p in keep:
        if p.__hash__ is not None: # object is hashable
            e[p] = d[p]
        elif isinstance(p, dict):
            for k in p:
                e[k] = dict_retain(d[k], p[k])
        else:
            raise Exception('Entries of list "keep" must either be hashable '\
                +'(i.e. able to be dict keys) or dict')
    return e
###############################################################################
def save_inventory(d_local, d_info_local, filename='', path='inventory', \
    fmt='json', detail='all'):
    '''
    Save local inventory info to a json or pkl file.
    '''
    if detail == 'all':
        pass
    elif isinstance(detail, (list,tuple)):
        d = {}
        for dataset in d_local:
            d[dataset] = dict_retain(d_local[dataset], detail)
        d_local = d
    else:
        raise Exception('Unknown detail level for saving inventory: {}'\
            .format(detail))
    ds = {'inventory' : d_local, 'info' : d_info_local}
    if not os.path.isdir(path): os.makedirs(path)
    fmt = fmt.strip('.')
    if filename == '':
        time_fmt = '%d%b%Y_%Hh%Mm%Ss'
        time_str = datetime.datetime.now().strftime(time_fmt) # time in local time zone
        filename = 'inventory_{}.{}'.format(time_str, fmt)
    else:
        filename, ext = os.path.splitext(filename)
        ext = ext.strip('.')
        if ext not in ['']:
            # If filename was supplied with an extension indicating the format,
            # use this as the format (if it's a valid format).
            if ext in ['json', 'pkl']:
                fmt = ext
        filename += '.' + fmt
    filepath = os.path.join(path, filename)
    if fmt == 'json':
        with open(filepath, 'w') as f:
            json.dump(ds, f, indent=2, sort_keys=True)
    elif fmt == 'pkl':
        with open(filepath, 'wb') as f:
            pickle.dump(ds, f, pickle.HIGHEST_PROTOCOL)
    else:
        raise Exception('Unknown format for saving inventory: {}'.format(fmt))
    print('Wrote inventory file: {}'.format(filepath))
###############################################################################
def load_inventory(filepath):
    '''
    Load an inventory that was saved by save_inventory().
    '''
    fmt = os.path.splitext(filepath)[-1].strip('.')
    if fmt == 'json':
        with open(filepath, 'r') as f:
            ds = json.load(f)
    elif fmt == 'pkl':
        with open(filepath, 'rb') as f:
            ds = pickle.load(f)
    print('Loaded inventory file: {}'.format(filepath))
    d_local = ds['inventory']
    d_info_local = ds['info']
    return d_local, d_info_local
###############################################################################
def check_local(datasets, d_found, d_local):
    '''
    Use inventory of local data archive to check if any dataset in list
    "datasets" is already downloaded. If it is (and if it's complete) then
    remove it from the list.
    
    Parameters
    ----------
    datasets : list
    
    d_found : dict
        ESGF search results
    
    d_local : dict
        Inventory of local archive
    
    '''
    if      len(datasets) == 0 \
        or  len(d_found) == 0 \
        or  len(d_local) == 0:
        return datasets

    checks = []
    checks.append('number_of_files')
    #checks.append('filenames')
    checks.append('size')

    print('\nChecking datasets list against local inventory...')
    l_remove = []
    for dataset in datasets:
        if dataset not in d_found: continue
        if dataset not in d_local: continue
        found = d_found[dataset]['doc']
        local = d_local[dataset]['doc']
        ok = True
        for check in checks:
            if check in ['number_of_files', 'size']:
                ok = found[check] == local[check]
            elif check == 'filenames':
                # Oops, the query results don't contain the filenames, just
                # the number of files. Could parse the wget script to get it.
                # (Or maybe there's a better way?) Leave it for now.
                pass
            else:
                assert False, 'Unknown check: ' + check
            if not ok: break
        if ok: l_remove.append(dataset)
    if len(l_remove) > 0:
        print('Removing these {0} datasets from the download list because they are already complete in the local archive:'.format(len(l_remove)))
        for dataset in l_remove:
            print ' '*2 + dataset
        datasets = [s for s in datasets if s not in l_remove]
    return datasets
###############################################################################
def retrieve_wget(d_found, wget_dir='./wget', datasets=None):
    '''
    Download wget scripts for datasets in d_found.
    
    Parameters
    ----------
    d_found : dict
    
    datasets : list
    
    wget_dir : str
    
    Returns
    -------
    d_wget_scripts : dict
        Maps datasets to the wget scripts that will download them.
    
    '''
    if len(d_found) == 0: return {}
    if datasets is None:
        # If datasets aren't specified, do all of them.
        l_dataset = sorted(d_found.keys(), key=lowercase)
    else:
        l_dataset = datasets
    if len(l_dataset) == 0: return {}

    print('\nRetrieving wget scripts...')
    docs = []
    for dataset in l_dataset:
        if dataset not in d_found:
            print('Dataset unavailable in d_found: {0}'.format(dataset))
            continue
        docs += [d_found[dataset]['doc']]
    d_wget_scripts = {}
    t_wget = time.time()
    for k,doc in enumerate(docs):
        #print k, doc['title']
        #urlname = doc['xlink'][1].split('|')[0]
        #print urlname

        '''
        https://esgf-node.llnl.gov/esg-search/wget/?distrib=false&dataset_id=CMIP6.CMIP.NCAR.CESM2.amip.r1i1p1f1.AERmonZ.ua.gn.v20190218|esgf-data.ucar.edu
        
        https://esgf-node.llnl.gov/esg-search/wget/?distrib=false&dataset_id=CMIP6.CMIP.NCAR.CESM2.amip.r1i1p1f1.AERmonZ.ua.gn.v20190218    
        
        
        CMIP6.CMIP.NCAR.CESM2.amip.r1i1p1f1.AERmonZ.ua.gn.v20190218|esgf-data.ucar.edu
        
        '''
        dataset_id = doc['id']
        #lw = ['https://esgf-node.llnl.gov/esg-search/wget/?distrib=false']
        wget_node = doc['index_node']
        lw = ['https://{}/esg-search/wget/?distrib=false'.format(wget_node)]
        lw += ['&dataset_id={}'.format(dataset_id)]
        urlname = ''.join(lw)
        
        dataset = str( dataset_id.split('|')[0] )
        wget_script = 'wget_' + dataset + '.sh'
        
        if not os.path.exists(wget_dir): os.makedirs(wget_dir)
        filepath = os.path.join(wget_dir, wget_script) 
        
        urllib.urlretrieve(urlname, filepath)
        os.system('chmod 744 ' + filepath)
        print('Saved wget script: ' + filepath)
        d_wget_scripts[dataset] = filepath

    t_wget = time.time() - t_wget
    print('\nTime taken to retrieve wget scripts: {0} s'.format('%.3g' % t_wget))
    return d_wget_scripts
###############################################################################
def inventory_wget(wget_dir='./wget', datasets=None):
    '''
    Look in directory wget_dir to determine what datasets exist. 
    '''
    d_wget_scripts = {}
    if not os.path.exists(wget_dir):
        #print('\nPath to wget scripts not found: ' + wget_dir)
        return d_wget_scripts
    if datasets is None:
        # Keep all datasets for which wget scripts are found in wget_dir
        filter_datasets = False
    else:
        # Retain only the datasets passed as input in list 'datasets'
        filter_datasets = True
        assert isinstance(datasets, (list, tuple))
    # Look for files with names as created by retrieve_wget()
    l = os.listdir(wget_dir)
    s_start = 'wget_'
    s_end = '.sh'
    l = [s for s in l if s.startswith(s_start)]
    l = [s for s in l if s.endswith(s_end)]
    for filename in l:
        dataset = filename.strip(s_start).strip(s_end)
        if filter_datasets:
            if dataset not in datasets:
                continue
        filepath = os.path.join(wget_dir, filename)
        d_wget_scripts[dataset] = filepath
    return d_wget_scripts    
###############################################################################
def setup_output_paths(local_archive, datasets, create_paths=True):
    '''
    In the specified local data archive (local_archive), set up directory tree
    in the CMIP6 format. 
    
    Parameters
    ----------
    local_archive : str
        Path (location) of local data archive. Downloaded datasets will end up
        here.
    
    datasets : list
        List of datasets for which to create directories in the local archive.
    
    create_paths : bool
        Flag determining if the paths are created. If not then this function
        is simply used to return the d_output_paths dict. 
    
    Returns
    -------
    d_output_paths : dict
        Maps datasets to their output paths.
    
    '''
    d_output_paths = {}
    if not os.path.exists(local_archive): return d_output_paths
    n_created = 0
    for dataset in datasets:
        d = dsd(dataset)
        if len(d) == 0:
            print('Unable to determine parameters to form output path for dataset: ' + dataset)
            continue
        d.update({'root' : local_archive})
        output_path = os.path.join(*[d[s] for s in OUTPUT_PATH_TEMPLATE])
        if not os.path.exists(output_path) and create_paths:
            #os.makedirs(output_path)
            # We need to set the permissions correctly for each dir level.
            # Go through the dir levels to do this.
            # (The os.makedirs() function is supposed to be able to do this
            # with its "mode" argument, but it doesn't work on our system.)
            l = [d[s] for s in OUTPUT_PATH_TEMPLATE]
            path = ''
            for m,s in enumerate(l):
                path = os.path.join(path, s)
                if not os.path.exists(path):
                    os.makedirs(path)
                    os.chmod(path, LOCAL_ARCHIVE_PERMISSIONS)
            n_created += 1
        d_output_paths[dataset] = output_path
    if create_paths and n_created > 0:
        print('\nDirectories for {0} datasets were created in {1}'.format(n_created, local_archive))
    return d_output_paths
###############################################################################
def write_wget_script(d_wget_scripts, d_output_paths, datasets=None, split_script=None, d_found=None, opt=None):
    '''
    Creates one or more shell scripts that will run the wget scripts, saving
    their output to the paths specified in d_output_paths.
    
    ADD DOC ABOUT HOW THE FILTERS IN SPLIT_SCRIPT WORK!
    
    '''
    if datasets is None:
        # If datasets aren't specified, do all of them.
        datasets = sorted(d_wget_scripts.keys(), key=lowercase)
    else:
        assert isinstance(datasets, (list, tuple))
    if len(datasets) == 0: return

    # Dict "opt" contains additional options. Set their default values here.
    if opt is None: opt = {}
    dfps(opt, {
        'certificate method' : '',
        'local archive as shell variable' : True,
        'create paths' : False,
        'echo info' : True,
        'disk info' : {},
    })
    script_stem = 'download_datasets'
    ext = '.sh'
    warn_about_disk_space = True
    l_script = []
    ###########################################################################
    if split_script is None:
        # All wget scripts will be put into the same master script.
        d_script = {
            'script'    : script_stem + ext,
            'datasets'  : datasets
        }
        l_script += [d_script]
    ###########################################################################
    else:
        # Split up the datasets into separate downloading scripts.
        # This can be useful if, e.g., we search for a set of variables
        # but want to have a separate master script for downloading each
        # variable. The split can be done by any parameter or combination
        # of parameters. A separate master script is created for each
        # unique combination of parameters (the axes definition in
        # summary() works the same way).
        
        # Set defaults
        dfps(split_script, {
            'filters'           : [],
            'parameter set'     : [],
        #    'parameter values'  : {},
        })
        lf = split_script['filters']
        lp = split_script['parameter set']
        #pv = split_script['parameter values']

        d_params = {}
        if d_found is not None: 
            for dataset in d_found:
                d_params[dataset] = dict( d_found[dataset]['params'] )
        
        allow_dataset_parameters_from_dataset_names = True
        if allow_dataset_parameters_from_dataset_names:
            # Try to get the dataset parameter values from the dataset name.
            # Which should be no problem as long as the dataset name is
            # complete. The reason to try this is that sometimes we re-run
            # the script without the original d_found being available. Or
            # perhaps d_found has been inadvertently deleted, but the wget
            # scripts are available. 
            for dataset in datasets:
                if dataset in d_params:
                    # Info on this dataset is already available
                    continue
                # Use the dataset name to determine the parameter values.
                d = dsd(dataset)
                if len(d) == 0:
                    # dsd() was unsuccessful in determining the parameter values
                    continue
                d_params[dataset] = d
        
        # Separate out any dataset for which we don't have info on its
        # parameters.
        unclassified = []
        for dataset in datasets:
            if dataset not in d_params: 
                unclassified += [dataset]
                continue
        datasets = [s for s in datasets if s not in unclassified]
        datasets0 = list(datasets) # remember original list of classifiable datasets
        
        dc = {} # dict to keep track of dataset groupings ("c" for "classification")
        lc = [] # list to keep track of groupings in the order in which they're created

        # First type of filter: split_script['filters'] is a list of filter
        # dicts, specified same way as for search filters used by search().
        # Each of these indicates datasets that are to be grouped into a
        # single script. Go through datasets and select out the ones 
        # belonging to these groups.
        for f in lf:
            f = dict(f)
            if len(f) == 0: continue
            for p in f:
                if isinstance(f[p], (str, unicode)):
                    f[p] = [f[p]]
                assert isinstance(f[p], (list, tuple))
                # f[p] is now a list of parameter values, for parameter p, that
                # are included in this group.
            ld = []
            for dataset in datasets:
                d = d_params[dataset] # the parameter values for this dataset
                # Check that all search parameters (in f) are available in d
                ok = True
                for p in f:
                    if p not in d:
                        ok = False
                        break
                # If any parameters aren't available, then skip this dataset
                if not ok: continue
                # Now see if the dataset belongs to the group specified by
                # filter f
                if all( [d[p] in f[p] for p in f] ):
                    # If all parameter values for this dataset were found in
                    # their corresponding f[p] lists, then the dataset belongs
                    # to group specified by filter f.
                    ld += [dataset]
            if len(ld) > 0:
                # Create a name for this grouping
                l = []
                for p in sorted(f.keys(), key=lowercase):
                    #l.append( '{0}={1}'.format(p, '+'.join(f[p])) )
                    l.append( '+'.join(f[p]) )
                c = '_'.join(l)
                if c not in dc:
                    # Add this group to the collection of groups
                    dc[c] = set(ld)
                    lc += [c]
            # Reduce datasets list to include only the ones not yet classified
            datasets = [s for s in datasets if s not in ld]
        
        # Second type of filter: find all unique combos of parameter values
        # for the specified parameter set.
        p_template = [p for p in DATASET_TEMPLATE if p in lp]
        # Go through datasets and classify them into groups based on
        # group specifications given by input dict split_script.
        for dataset in datasets:
            # Find the classification that applies to this dataset
            d = {}
            for p in lp:
                d[p] = d_params[dataset][p]
            # Create label for the classification
            c = str( SEP_DATASET.join([d[p] for p in p_template]) )
            if c not in dc:
                # If this classification doesn't exist yet, create it
                dc[c] = set()
                lc += [c]
            # Add the dataset to its group
            dc[c].add(dataset)

        if not True:
            print dc.keys()
            for c in lc:
                print dc[c]
        
        #for c in sorted(dc.keys(), key=lowercase):
        done = []
        for c in lc:
            datasets = sorted(list(dc[c]), key=lowercase)
            datasets = [s for s in datasets if s not in done]
            if c == '': 
                if set(datasets0) == set(datasets):
                    # This script includes all classifiable datasets.
                    s = ''
                else:
                    # This script includes the leftovers after datasets
                    # were classified.
                    s = '_other'
            else: 
                s = '_' + c
            d_script = {
                'script'    : script_stem + s + ext,
                'datasets'  : datasets
            }
            l_script += [d_script]
            done += datasets

        # Ensure we didn't miss any datasets
        unclassified += [s for s in datasets0 if s not in done]
            
        if len(unclassified) > 0:
            # If the above partitioning somehow missed any dataset
            # then put it in a final script containing these unclassified
            # datasets, just to ensure we don't miss any. I don't expect
            # this to happen, just being safe.
            d_script = {
                'script'    : script_stem + '_unclassified' + ext,
                'datasets'  : datasets
            }
            l_script += [d_script]
    ###########################################################################
    print('\nWriting shell scripts to run the wget scripts...')
    
    use_path_var = opt['local archive as shell variable']
    # use_path_var=True ==> in the shell script show the local archive (top dir) as a shell variable.
    # This allows user to easily re-run the script to put the output in a different place if desired.
    # It also makes the download script more readable.
    
    local_archive0 = os.path.abspath(opt['local archive'])

    certificate_method = opt['certificate method']
    # Sometimes we need to get around the use of certificates, for whatever reason.
    # It seems (6 Aug 2019) that on the ECCC ppp machines this is possible using Mike Brady's workaround.
    # On my laptop this workaround seems not to work, but using the -i flag does work.
    # However the -i flag doesn't work on the ppp's!
    
    total_size_all_downloads = 0
    for d_script in l_script:
        script   = d_script['script']
        datasets = d_script['datasets']
        lw = []
        time_fmt = '%Y-%m-%d %H:%M:%S'
        lw.append('# Download script generated at ' + datetime.datetime.now().strftime(time_fmt))
        # Find out how big the download will be.
        n_known = 0
        n_unknown = 0
        total_size = 0
        skip_datasets = set()
        for dataset in datasets:
            if dataset not in d_output_paths:
                print('No output path defined for dataset: ' + dataset)
                skip_datasets.add(dataset)
                continue
            if dataset not in d_wget_scripts:
                print('No wget script available for dataset: ' + dataset)
                skip_datasets.add(dataset)
                continue
            if dataset in d_found:
                total_size += d_found[dataset]['doc']['size']
                n_known += 1
            else:
                n_unknown += 1
        if n_known > 0:
            lw.append('# Will download {0} datasets, total size: {1}'.format(n_known, file_size_str(total_size)))
        if n_unknown > 0:
            lw.append('# Will download {0} datasets whose size could not be determined'.format(n_unknown))
        datasets = [s for s in datasets if s not in skip_datasets]
        n_download = n_known + n_unknown
        assert len(datasets) == n_download
        nw = 0
        if certificate_method == 'pass OpenID':
            # Set shell variables containing the OpenID and password.
            # This allows possibility of user changing them later in one place
            # at the top of the script.
            # The OpenID must be given in the full form, e.g. 'https://ceda.ac.uk/openid/James.Anstey'
            lw.append('openid=' + opt['OpenID'])
            lw.append('openid_pw=' + opt['OpenID password'])
        if use_path_var:
            # Set shell variable containing the local archive absolute path.
            local_archive_shell_var = 'local_archive'
            lw.append('{0}={1}'.format(local_archive_shell_var, local_archive0))
        #for dataset in datasets:
        for nd, dataset in enumerate(datasets):
            if dataset in d_found:
                dataset_size = file_size_str( d_found[dataset]['doc']['size'] )
                num_files = d_found[dataset]['doc']['number_of_files']
            else:
                dataset_size = 'unknown'
                num_files = 'unknown'
            lw.append('# dataset: {0}, size: {1}, no. of files: {2}'.format(dataset, dataset_size, num_files))
            output_path = d_output_paths[dataset]
            output_path = os.path.abspath(output_path)
            if use_path_var:
                assert output_path.startswith(local_archive0) and output_path.count(local_archive0) == 1
                output_path = output_path.replace(local_archive0, '$' + local_archive_shell_var)
                local_archive = '$' + local_archive_shell_var
            else:
                local_archive = local_archive0
            if opt['create paths']:
                relpath = os.path.relpath(output_path, local_archive)
                relpath_top_dir = relpath.split(os.path.sep)[0]
                path = os.path.join(local_archive, relpath_top_dir)
                lw.append('mkdir -p ' + output_path)
                local_archive_permissions_str = oct(LOCAL_ARCHIVE_PERMISSIONS)[1:] # changes 0775 to '775'
                lw.append('chmod -R {0} {1} > /dev/null 2>&1'.format(local_archive_permissions_str, path))
                # "> /dev/null 2>&1" is added because otherwise this command
                # can lead to a lot of "permission denied" messages being
                # displayed on the stdout. Even if not displayed, these
                # attempts at changing the permissions of subdirs still slow
                # the script down. For this reason it's better to not use
                # this option and instead have setup_output_paths() create
                # the dirs. That could potentially create a lot of empty dirs
                # (if the user chooses not to download all the data for which
                # dirs were created) but the inventory function will ignore
                # empty dirs so this is not a big deal.
            wget_path, wget_script = os.path.split(d_wget_scripts[dataset])
            wget_path = os.path.abspath(wget_path)
            if opt['echo info']:
                # Put commands in the shell script that will echo some info
                # to stdout. It clutters up the shell script a little, but
                # helps keep user informed while the shell script is running.
                lwe = []
                lwe.append( ('downloading dataset {0} of {1}'.format(nd+1,n_download), dataset) )
                lwe.append( ('destination directory', output_path) )
                lwe.append( ('dataset size', dataset_size) )
                lwe.append( ('number of files', num_files) )
                m = max([len(t[0]) for t in lwe])
                fmt = '%-'+str(m)+'s'
                lwe = ['{0} : {1}'.format(fmt % t[0], t[1]) for t in lwe]
                indent = ' '*4
                lwe = [indent + s for s in lwe]
                lwe[0] = '\\n' + lwe[0]
                lwe[-1] += '\\n'
                for s in lwe:
                    lw.append('echo -e "' + s + '"')
            lw.append('cd ' + output_path)
            wget_scriptpath = os.path.join(wget_path, wget_script)
            if certificate_method == 'pass OpenID':
                cmd = ' '.join(['echo', '$openid_pw', '|', \
                    wget_scriptpath, '-H', '$openid'])
            elif certificate_method == '-i':
                cmd = wget_scriptpath + ' -i'
            else:
                cmd = wget_scriptpath
            lw.append(cmd)
            # Set permissions of the downloaded files so they're readable by anyone
            #lw.append('chmod 444 *')
            #lw.append('chmod 644 *') # user write permission is required for re-download attempts
            lw.append('chmod ga+r *') 
            # ga+r permission ensures anyone can read the files (since the data may go into a
            # shared group archive) and leaves the user permissions as whatever ESGF
            # thinks they should be.
            
            # Remove the wget status file, it doesn't seem to contain useful info
            #lw.append('rm .wget*.status')
            # Oops - do NOT remove this file. It's what wget may use to determine if
            # the dataset is already downloaded & validated when you re-run wget.
            # It's not totally clear whether it's needed: when I download CCCma data
            # it seems to be required, but when I tested using NCAR data it didn't
            # matter if this file was present. Seems safest to leave it alone for now.
            # (28 Aug 2019)

            nw += 1 # count of datasets added to the download script

        if nw > 0:
            with open(script, 'w') as f:
                f.write('\n'.join(lw))
            os.system('chmod 744 ' + script)
            print('Wrote script to download {0} datasets ({1} total): {2}'.format(nw, file_size_str(total_size), script))
            if n_unknown > 0:
                print('WARNING: {0} of these datasets are of unknown size'.format(n_unknown))
            total_size_all_downloads += total_size
        else:
            print('No datasets available for script: {0}'.format(script))
            assert total_size == 0

    print('\nTotal size for all downloads: {0}'.format(file_size_str(total_size_all_downloads)))
    if warn_about_disk_space and len(opt['disk info']) > 0:
        # Compare total size of downloads to available disk space in the local
        # archive. Tell user how much space will be left available if all the
        # data is downloaded. Warn user if there's not enough room.
        show_disk_usage(opt['disk info'])
        # (disk usage summary should have been displayed earlier by search.py,
        # but user could easily miss it since there can be lots of other stuff
        # on the stdout, e.g. the table summaries)
        warning_threshold = 0.02
        available_space = opt['disk info']['available disk space']
        total_space     = opt['disk info']['total disk space']
        percent_used    = opt['disk info']['total disk space']
        space_left_after_download = available_space - total_size_all_downloads
        if   space_left_after_download <= 0:
            print('WARNING: total download size exceeds available disk space!')
        elif space_left_after_download < warning_threshold*total_space:
            print('WARNING: after download, less than {0}% of total disk space ({1}) will remain available.'\
                .format(100*warning_threshold, \
                file_size_str(space_left_after_download)))
        else:
            print('The downloads will use up {0}% of available disk space.'\
                .format('%.3g' % float( 1e2*total_size_all_downloads/available_space )))
            print('After all downloads, remaining available space will be {0}.'\
                .format(file_size_str(space_left_after_download)))
###############################################################################
def get_openid():
    '''
    Get the user's OpenID and password. First look for a file (.openid.json)
    in the repo's search/ directory containing the info. If not found then it
    queries the user for the info and creates the file.
    '''
    filepath = '../.openid.json'
    openid_found = False
    if os.path.exists(filepath):
        with open(filepath, 'r') as f:
            d = json.load(f)
        if ('OpenID' in d) and ('OpenID password' in d):
            openid = d['OpenID']
            openid_pw = d['OpenID password']
            openid_found = True
    if not openid_found:
        # OpenID wasn't found in a file, so query the user for it.
        ok = False
        while not ok:
            openid = raw_input('Enter your OpenID (the full form, beginning with "https://..."): ')
            openid_pw = raw_input('Enter your OpenID password: ')
            print('OpenID: ' + openid)
            print('OpenID password: ' + openid_pw)
            ok = raw_input('Are these correct? (enter "y" or "yes" if they are) ')
            ok = ok.lower() in ['y', 'yes']
        d = {'OpenID' : openid, 'OpenID password' : openid_pw}
        # Create the file containing the OpenID (for future use, so that
        # user doesn't have to be prompted for it again).
        with open(filepath, 'w') as f:
            w = json.dumps(d, indent=2, sort_keys=True)
            f.write(w)
            os.chmod(filepath, 0400)
            print 'Wrote file to store OpenID: ' + filepath
    return openid, openid_pw
###############################################################################
